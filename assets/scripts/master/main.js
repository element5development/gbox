var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		NOTIFICATION BAR
	\*----------------------------------------------------------------*/
	if (readCookie('cookieNotification') === 'false') {
		$('.cookie-useage-notification').removeClass("note-on");
	} else {
		$('.cookie-useage-notification').addClass("note-on");
	}

	$('.cookie-useage-notification button').click(function () {
		$('.cookie-useage-notification').removeClass("note-on");
		createCookie('cookieNotification', 'false');
	});

	function createCookie(name, value, days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		}
		document.cookie = name + "=" + value + expires + "; path=/";
	}

	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') c = c.substring(1, c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
		}
		return null;
	}
	/*----------------------------------------------------------------*\
		SELECT FIELD PLACEHOLDER
	\*----------------------------------------------------------------*/
	$(function () {
		$('select').addClass('has-placeholder');
	});
	$("select").change(function () {
		$(this).removeClass('has-placeholder');
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	$('label').each(function () {
		if ($(this).siblings('.ginput_container_fileupload').length) {
			$(this).addClass('file-upload-label');
		}
	});

	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			$('label.file-upload-label').addClass("file-uploaded");
		});
	}
	/*-----------------------------------*\
		SEARCH POPUP
	\*-----------------------------------*/
	$(".search-toggle").click(function () {
		$(".search-popup").toggleClass("is-active");
	});
	$('.search-popup').on('click', function (e) {
		if (e.target !== this) {
			// do nothing
		} else {
			$(".search-popup").toggleClass("is-active");
		}
	});
	/*-----------------------------------*\
		MOBILE MENU POPUP
	\*-----------------------------------*/
	$(".mobile-menu-toggle").click(function () {
		$('html').toggleClass("cannot-scroll");
		$(this).text(function (i, text) {
			return text === "Menu" ? "Close" : "Menu";
		})
		$(".mobile-wrap").toggleClass("is-active");
	});
	/*-----------------------------------*\
		JOIN POPUP
	\*-----------------------------------*/
	$(".join-toggle").click(function () {
		$(".join-popup").toggleClass("is-active");
	});
	$('.join-popup').on('click', function (e) {
		if (e.target !== this) {
			// do nothing
		} else {
			$(".join-popup").toggleClass("is-active");
		}
	});
	/*-----------------------------------*\
		DISABLE LINKS
	\*-----------------------------------*/
	$('.functional a').click(function (e) {
		e.preventDefault();
	});
	/*-----------------------------------*\
		HOME HEADER
	\*-----------------------------------*/
	$('.front-head .monthly').hover(function () {
			$('.front-head').addClass('monthly-active');
			$('.front-head .monthly .button').addClass('is-blue');
		},
		function () {
			$('.front-head').removeClass('monthly-active');
			$('.front-head .monthly .button').removeClass('is-blue');
		});
	$('.front-head .premium').hover(function () {
			$('.front-head').addClass('premium-active');
			$('.front-head .premium .button').addClass('is-red');
		},
		function () {
			$('.front-head').removeClass('premium-active');
			$('.front-head .premium .button').removeClass('is-red');
		});
	/*-----------------------------------*\
		TESTIMONY SLIDER
	\*-----------------------------------*/
	$('.testimonies').slick({
		prevArrow: '<button type="button" class="slick-prev"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M8.951 22.39a.718.718 0 0 0 .393-.64V16.72H31.28a.719.719 0 1 0 0-1.438H9.344v-5.03a.718.718 0 0 0-1.142-.582l-7.906 5.75a.72.72 0 0 0 0 1.163l7.906 5.75a.719.719 0 0 0 .75.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M23.049 9.61a.718.718 0 0 0-.393.64v5.031H.72a.719.719 0 1 0 0 1.438h21.937v5.03a.718.718 0 0 0 1.142.582l7.906-5.75a.72.72 0 0 0 0-1.163l-7.906-5.75a.719.719 0 0 0-.75-.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		fade: true,
		cssEase: 'linear',
		autoplay: true,
		autoplaySpeed: 6000,
		infinite: true,
	}).on('setPosition', function (event, slick) {
		slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});
	/*-----------------------------------*\
		FEATURED PRODUCTS
	\*-----------------------------------*/
	$('.featured-products .products').slick({
		prevArrow: '<button type="button" class="slick-prev"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M8.951 22.39a.718.718 0 0 0 .393-.64V16.72H31.28a.719.719 0 1 0 0-1.438H9.344v-5.03a.718.718 0 0 0-1.142-.582l-7.906 5.75a.72.72 0 0 0 0 1.163l7.906 5.75a.719.719 0 0 0 .75.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M23.049 9.61a.718.718 0 0 0-.393.64v5.031H.72a.719.719 0 1 0 0 1.438h21.937v5.03a.718.718 0 0 0 1.142.582l7.906-5.75a.72.72 0 0 0 0-1.163l-7.906-5.75a.719.719 0 0 0-.75-.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		autoplay: true,
		autoplaySpeed: 3000,
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		responsive: [{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	}).on('setPosition', function (event, slick) {
		slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});
	/*-----------------------------------*\
		QUANTITY UPDATE
	\*-----------------------------------*/
	$('.summary .quantity input').after('<button type="button" class="negative">-</button>');
	$('.summary .quantity input').before('<button type="button" class="positive">+</button>');
	$(function () {
		$('.summary .quantity .positive').on('click', function () {
			var $qty = $(this).parent().find('.qty');
			var currentVal = parseInt($qty.val());
			if (!isNaN(currentVal)) {
				$qty.val(currentVal + 1);
			}
		});
		$('.summary .quantity .negative').on('click', function () {
			var $qty = $(this).parent().find('.qty');
			var currentVal = parseInt($qty.val());
			if (!isNaN(currentVal) && currentVal > 0) {
				$qty.val(currentVal - 1);
			}
		});
	});
	/*-----------------------------------*\
		PAST BOXES
	\*-----------------------------------*/
	$('.past-boxes .boxes').slick({
		prevArrow: '<button type="button" class="slick-prev"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M8.951 22.39a.718.718 0 0 0 .393-.64V16.72H31.28a.719.719 0 1 0 0-1.438H9.344v-5.03a.718.718 0 0 0-1.142-.582l-7.906 5.75a.72.72 0 0 0 0 1.163l7.906 5.75a.719.719 0 0 0 .75.059z" fill="#ffffff" fill-rule="nonzero"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M23.049 9.61a.718.718 0 0 0-.393.64v5.031H.72a.719.719 0 1 0 0 1.438h21.937v5.03a.718.718 0 0 0 1.142.582l7.906-5.75a.72.72 0 0 0 0-1.163l-7.906-5.75a.719.719 0 0 0-.75-.059z" fill="#ffffff" fill-rule="nonzero"/></svg></button>',
		fade: true,
		cssEase: 'linear',
		autoplay: true,
		autoplaySpeed: 6000,
		infinite: true,
	}).on('setPosition', function (event, slick) {
		slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});
	/*-----------------------------------*\
		COMPARISON TESTIMONY
	\*-----------------------------------*/
	$('.comparison-testimonies').slick({
		prevArrow: '<button type="button" class="slick-prev"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M8.951 22.39a.718.718 0 0 0 .393-.64V16.72H31.28a.719.719 0 1 0 0-1.438H9.344v-5.03a.718.718 0 0 0-1.142-.582l-7.906 5.75a.72.72 0 0 0 0 1.163l7.906 5.75a.719.719 0 0 0 .75.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M23.049 9.61a.718.718 0 0 0-.393.64v5.031H.72a.719.719 0 1 0 0 1.438h21.937v5.03a.718.718 0 0 0 1.142.582l7.906-5.75a.72.72 0 0 0 0-1.163l-7.906-5.75a.719.719 0 0 0-.75-.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 6000,
		infinite: true,
		responsive: [{
			breakpoint: 1050,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}],
	});

	/*-----------------------------------*\
		COMMUNITY SLIDER
	\*-----------------------------------*/
	$('.community-gallery').slick({
		prevArrow: '<button type="button" class="slick-prev"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M8.951 22.39a.718.718 0 0 0 .393-.64V16.72H31.28a.719.719 0 1 0 0-1.438H9.344v-5.03a.718.718 0 0 0-1.142-.582l-7.906 5.75a.72.72 0 0 0 0 1.163l7.906 5.75a.719.719 0 0 0 .75.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		nextArrow: '<button type="button" class="slick-next"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M23.049 9.61a.718.718 0 0 0-.393.64v5.031H.72a.719.719 0 1 0 0 1.438h21.937v5.03a.718.718 0 0 0 1.142.582l7.906-5.75a.72.72 0 0 0 0-1.163l-7.906-5.75a.719.719 0 0 0-.75-.059z" fill="#5b5b5b" fill-rule="nonzero"/></svg></button>',
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 6000,
		infinite: true,
		responsive: [{
				breakpoint: 1050,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		],
	});

	/*-----------------------------------*\
		GIFT FORM LOGIC
	\*-----------------------------------*/
	$(".ship-us-toggle").click(function () {
		$("#subscriptions .recommendation:not([data-shipping*='us'])").removeClass("has-accurate-shipping");
		$("#subscriptions .recommendation[data-shipping*='us']").addClass("has-accurate-shipping");
		$("#style .options .option:last-child").removeClass("is-disabled");
		$('.explination').addClass('shipping-answered');
	});
	$(".ship-can-toggle").click(function () {
		$("#subscriptions .recommendation:not([data-shipping*='can'])").removeClass("has-accurate-shipping");
		$("#subscriptions .recommendation[data-shipping*='can']").addClass("has-accurate-shipping");
		$("#style .options .option:last-child").removeClass("is-disabled");
		$('.explination').addClass('shipping-answered');
	});
	$(".ship-int-toggle").click(function () {
		$("#subscriptions .recommendation:not([data-shipping*='int'])").removeClass("has-accurate-shipping");
		$("#subscriptions .recommendation[data-shipping*='int']").addClass("has-accurate-shipping");
		$("#subscriptions .recommendation[data-shipping*='int']").addClass("has-accurate-shipping");
		$("#style .options .option:last-child").addClass("is-disabled");
		$('.explination').addClass('shipping-answered');
	});

	$(".pricing-one").click(function () {
		$("#subscriptions .recommendation:not([data-pricing*='1'])").removeClass("has-accurate-pricing");
		$("#subscriptions .recommendation[data-pricing*='1']").addClass("has-accurate-pricing");
		$('.explination').addClass('pricing-answered');
	});
	$(".pricing-two").click(function () {
		$("#subscriptions .recommendation:not([data-pricing*='2'])").removeClass("has-accurate-pricing");
		$("#subscriptions .recommendation[data-pricing*='2']").addClass("has-accurate-pricing");
		$('.explination').addClass('pricing-answered');
	});
	$(".pricing-three").click(function () {
		$("#subscriptions .recommendation:not([data-pricing*='3'])").removeClass("has-accurate-pricing");
		$("#subscriptions .recommendation[data-pricing*='3']").addClass("has-accurate-pricing");
		$('.explination').addClass('pricing-answered');
	});
	$(".pricing-four").click(function () {
		$("#subscriptions .recommendation:not([data-pricing*='4'])").removeClass("has-accurate-pricing");
		$("#subscriptions .recommendation[data-pricing*='4']").addClass("has-accurate-pricing");
		$('.explination').addClass('pricing-answered');
	});

	$(".style-classic").click(function () {
		$("#subscriptions .recommendation:not([data-type='classic'])").removeClass("has-accurate-style");
		$("#subscriptions .recommendation[data-type='classic']").addClass("has-accurate-style");
		$('.explination').addClass('style-answered');
	});
	$(".style-premium").click(function () {
		$("#subscriptions .recommendation:not([data-type='premium'])").removeClass("has-accurate-style");
		$("#subscriptions .recommendation[data-type='premium']").addClass("has-accurate-style");
		$('.explination').addClass('style-answered');
	});

	$('.ship-us-toggle').click(function () {
		$('.explination > p:first-of-type span:first-child').text('United States');
	});
	$('.ship-can-toggle').click(function () {
		$('.explination > p:first-of-type span:first-child').text('Canada');
	});
	$('.ship-int-toggle').click(function () {
		$('.explination > p:first-of-type span:first-child').text('International');
	});
	$('.pricing-one').click(function () {
		$('.explination > p:first-of-type span:nth-child(2)').text('Under $100');
	});
	$('.pricing-two').click(function () {
		$('.explination > p:first-of-type span:nth-child(2)').text('$100 - $200');
	});
	$('.pricing-three').click(function () {
		$('.explination > p:first-of-type span:nth-child(2)').text('$200 - $300');
	});
	$('.pricing-four').click(function () {
		$('.explination > p:first-of-type span:nth-child(2)').text('Over $300');
	});
	$('.style-classic').click(function () {
		$('.explination > p:first-of-type span:last-child').text('Fashion Accessories');
		$('.explination > h2 span').text('Classic');
		$('.premium-description').addClass('is-hidden');
		$('.classic-description').removeClass('is-hidden');
	});
	$('.style-premium').click(function () {
		$('.explination > p:first-of-type span:last-child').text('Lifestyle Products');
		$('.explination > h2 span').text('Premium');
		$('.classic-description').addClass('is-hidden');
		$('.premium-description').removeClass('is-hidden');
	});
});
/*-----------------------------------*\
	SUSPEND BUTTON TEXT
\*-----------------------------------*/
$("a.button.suspend").html('Skip Next Box');
/*-----------------------------------*\
	REACTIVATE BUTTON TEXT
\*-----------------------------------*/
$("a.button.reactivate").html('Receive Next Box');
/*-----------------------------------*\
	CALL TO CANCEL SUBSCRIPTION
\*-----------------------------------*/
$('.woocommerce-MyAccount-content table.subscription_details').after('Please email us at <a href="mailto:support@gentlemansbox.com">support@gentlemansbox.com</a> to cancel your subscription.</p>');
/*-----------------------------------*\
-	SHIPPING ADDRESS TOGGLE
-\*-----------------------------------*/
$('#ship-to-different-address > label').change(function () {
	$(this).toggleClass("is-active");
});
/*-----------------------------------*\
	FLIP ADDRESS FIELDS AT CHECKOUT
\*-----------------------------------*/
// jQuery('.shipping_address').css('display', 'block');
// var bigHtml = jQuery(".woocommerce-billing-fields").children().not("h3, .create-account").detach();
// var smallHtml = jQuery('.shipping_address').detach();
// jQuery('.woocommerce-billing-fields').append(smallHtml);
// jQuery('#order_comments_field').before(bigHtml);
// jQuery('.woocommerce-billing-fields h3').text('Shipping Details');
// jQuery('h3#ship-to-different-address').replaceWith('<h3 id="ship-to-billing-different-address">Billing Address</h3>');