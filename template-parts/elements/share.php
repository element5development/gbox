<ul class="social-share">
	<li>
		<a target="_blank" href="mailto:?body=Check%20out%20this%20out%20from%20Gentleman's%20Box.%20%0A<?php echo get_page_link(); ?>">
			<svg>
				<title>Email</title>
				<use xlink:href="#email" />
			</svg>
		</a>
	</li>
	<li>
		<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_page_link(); ?>">
			<svg>
				<title>Facebook</title>
				<use xlink:href="#fb" />
			</svg>
		</a>
	</li>
	<li>
		<a target="_blank" href="https://twitter.com/intent/tweet?text=Check%20out%20this%20<?php echo strip_tags(get_the_title()); ?>%20from%20Gentleman's%20Box.%20<?php echo get_page_link(); ?>">
			<svg>
				<title>Twitter</title>
				<use xlink:href="#twitter" />
			</svg>
		</a>
	</li>
</div>