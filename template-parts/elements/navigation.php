<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<?php 
	$menuParameters = array(
		'container'       => false,
		'echo'            => false,
		'items_wrap'      => '%3$s',
		'depth'           => 0,
		'theme_location' => 'primary_navigation'
	);
?>
<div class="primary-navigation">
	<nav>
		<a class="logo" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-horizontal.svg" alt="Back to Homepage" />
		</a>
		<button class="join-toggle">Join</button>
		<button class="mobile-menu-toggle">Menu</button>
		<div class="mobile-wrap">
			<button class="join-toggle">Join</button>
			<?php echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' ); ?>
			<a class="account" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
				<svg>
					<title>View Account</title>
					<use xlink:href="#account" />
				</svg>
			</a>
			<a class="cart" href="<?php echo get_permalink( wc_get_page_id( 'cart' ) ); ?>">
				<?php if ( WC()->cart->get_cart_contents_count() > 0  ) : ?>
					<div class="counter">
						<?php echo WC()->cart->get_cart_contents_count(); ?>
					</div>
				<?php endif; ?>
				<svg>
					<title>View Cart</title>
					<use xlink:href="#cart" />
				</svg>
			</a>
			<button class="search-toggle">
				<svg>
					<title>Open Search Form</title>
					<use xlink:href="#search" />
				</svg>
			</button>
		</div>
	</nav>
</div>

<div class="search-popup">
	<div>
		<button class="search-toggle">
			<svg>
				<title>Close Search Form</title>
				<use xlink:href="#close" />
			</svg>
		</button>
		<?php echo get_search_form(); ?>
	</div>
</div>

<div class="join-popup">
	<div>
		<button class="join-toggle">
			<svg>
				<title>Close Join Options</title>
				<use xlink:href="#close" />
			</svg>
		</button>
		<div class="classic">
			<div>
				<h2>Must have accessories</h2>
				<p>
					<b>Past Boxes Included</b><br/>
					Socks | Ties | Cufflinks | Pocket Squares<br/>Tie Clips | Grooming Samples
				</p>
				<p>$35 per box and delivered <b>every month</b></p>
				<a href="<?php the_permalink(418); ?>" class="button is-blue">Join CLassic</a>
			</div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/classic-featured-example.jpg" alt="classic box containing various fahsion accessories" />
		</div>
		<div class="premium">
			<div>
				<h2>Live the lifestyle</h2>
				<p>
					<b>Past Boxes Included</b><br/>
					Leather Goods | Electronics | Luxury Watches<br/>Premium Home-ware
				</p>
				<p>$119 per box and delivered <b>every 3 months</b></p>
				<a href="<?php the_permalink(399); ?>" class="button is-red">Join Premium</a>
			</div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/premium-featured-example.jpg" alt="premium box containing various life and fashion accessories" />
		</div>
	</div>
</div>