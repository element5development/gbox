<?php 
/*----------------------------------------------------------------*\

	SVG ICONS
	List of all the svg icons used throughout the site.
	Allows for easy access without repeat code.

\*----------------------------------------------------------------*/
?>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none">
	<symbol id="account" viewBox="0 0 32 32">
		<g class="nc-icon-wrapper">
			<path data-color="color-2" d="M23.309 21.867c-1.192-.387-1.979-1.184-2.506-2.1A9.933 9.933 0 0 1 16 21a9.942 9.942 0 0 1-4.803-1.233c-.527.917-1.314 1.714-2.505 2.101C4.638 23.183 1 24.871 1 31v1h30v-1c0-6.129-3.638-7.817-7.691-9.133z"/><path d="M16 0c-4.411 0-8 3.589-8 8v3c0 4.411 3.589 8 8 8s8-3.589 8-8V8c0-4.411-3.589-8-8-8z"/>
		</g>
	</symbol>
	<symbol id="cart" viewBox="0 0 32 32">
		<g class="nc-icon-wrapper">
			<circle data-color="color-2" cx="5" cy="29" r="3"/>
			<circle data-color="color-2" cx="27" cy="29" r="3"/>
			<path d="M31 22H2.767l1.8-3H24c.414 0 .785-.255.934-.641l5-13A1.002 1.002 0 0 0 29 4H4.5L1.8.4A1 1 0 1 0 .2 1.6L3 5.333v12.39L.143 22.485A1 1 0 0 0 1 24h30a1 1 0 1 0 0-2z"/>
		</g>
	</symbol>
	<symbol id="search" viewBox="0 0 32 32">
		<path d="M30.414 27.586l-7.644-7.644a12.038 12.038 0 1 0-2.828 2.828l7.644 7.644zM3 13a10 10 0 1 1 10 10A10.011 10.011 0 0 1 3 13z"/>
	</symbol>
	<symbol id="close" viewBox="0 0 16 16">
		<path d="M10.1 4.5L8 6.6 5.9 4.5 4.5 5.9 6.6 8l-2.1 2.1 1.4 1.4L8 9.4l2.1 2.1 1.4-1.4L9.4 8l2.1-2.1z"/>
	</symbol>
	<symbol id="fb" viewBox="0 0 32 32">
		<path d="M12.462 31V18H8v-6h4.462V7.81c0-4.564 2.89-6.81 6.961-6.81 1.95 0 3.627.145 4.115.21v4.77l-2.824.001C18.5 5.981 18 7.034 18 8.578V12h6l-2 6h-4v13h-5.538z" fill="#535353"/>
	</symbol>
	<symbol id="twitter" viewBox="0 0 48 48">
		<path fill="#535353" d="M48 9.113a19.686 19.686 0 0 1-5.656 1.551 9.876 9.876 0 0 0 4.33-5.448 19.717 19.717 0 0 1-6.253 2.39 9.835 9.835 0 0 0-7.189-3.11c-5.438 0-9.848 4.409-9.848 9.847 0 .772.087 1.524.255 2.244-8.184-.41-15.44-4.33-20.297-10.289a9.801 9.801 0 0 0-1.334 4.951 9.844 9.844 0 0 0 4.381 8.197 9.808 9.808 0 0 1-4.46-1.232l-.001.124c0 4.771 3.394 8.752 7.9 9.656a9.867 9.867 0 0 1-4.448.169 9.858 9.858 0 0 0 9.2 6.839 19.76 19.76 0 0 1-12.23 4.216c-.796 0-1.58-.047-2.35-.138a27.874 27.874 0 0 0 15.096 4.424c18.113 0 28.019-15.005 28.019-28.019 0-.427-.01-.851-.029-1.274A20.014 20.014 0 0 0 48 9.113z"/>
	</symbol>
	<symbol id="insta" viewBox="0 0 48 48">
		<g class="nc-icon-wrapper" fill="#535353">
			<path d="M24 4.324c6.408 0 7.167.024 9.698.14 2.731.125 5.266.672 7.216 2.622 1.95 1.95 2.497 4.485 2.622 7.216.115 2.531.14 3.29.14 9.698s-.024 7.167-.14 9.698c-.125 2.731-.672 5.266-2.622 7.216-1.95 1.95-4.485 2.497-7.216 2.622-2.53.115-3.289.14-9.698.14s-7.168-.024-9.698-.14c-2.731-.125-5.266-.672-7.216-2.622-1.95-1.95-2.497-4.485-2.622-7.216-.115-2.531-.14-3.29-.14-9.698s.024-7.167.14-9.698c.125-2.731.672-5.266 2.622-7.216 1.95-1.95 4.485-2.497 7.216-2.622 2.531-.115 3.29-.14 9.698-.14M24 0c-6.518 0-7.335.028-9.895.144-3.9.178-7.326 1.133-10.077 3.884C1.278 6.778.322 10.203.144 14.105.028 16.665 0 17.482 0 24s.028 7.335.144 9.895c.178 3.9 1.133 7.326 3.884 10.077 2.75 2.75 6.175 3.706 10.077 3.884 2.56.116 3.377.144 9.895.144s7.335-.028 9.895-.144c3.899-.178 7.326-1.133 10.077-3.884 2.75-2.75 3.706-6.175 3.884-10.077.116-2.56.144-3.377.144-9.895s-.028-7.335-.144-9.895c-.178-3.9-1.133-7.326-3.884-10.077-2.75-2.75-6.175-3.706-10.077-3.884C31.335.028 30.518 0 24 0z"/>
			<path data-color="color-2" d="M24 11.676c-6.807 0-12.324 5.518-12.324 12.324S17.193 36.324 24 36.324 36.324 30.807 36.324 24 30.807 11.676 24 11.676zM24 32a8 8 0 1 1 0-16 8 8 0 0 1 0 16z"/><circle data-color="color-2" cx="36.811" cy="11.189" r="2.88"/>
		</g>
	</symbol>
	<symbol id="youtube" viewBox="0 0 48 48">
		<g class="nc-icon-wrapper">
			<path fill="#535353" d="M47.52 14.403s-.468-3.308-1.908-4.764c-1.825-1.912-3.87-1.922-4.809-2.034C34.086 7.12 24.01 7.12 24.01 7.12h-.02s-10.076 0-16.793.485c-.938.112-2.984.122-4.81 2.034C.949 11.095.48 14.403.48 14.403S0 18.287 0 22.172v3.641c0 3.884.48 7.769.48 7.769s.468 3.308 1.908 4.764c1.825 1.912 4.224 1.852 5.292 2.052 3.84.368 16.32.482 16.32.482s10.086-.015 16.803-.5c.938-.113 2.984-.122 4.81-2.034 1.439-1.456 1.908-4.764 1.908-4.764S48 29.697 48 25.813v-3.641c0-3.885-.48-7.769-.48-7.769z"/>
			<path fill="#FFF" d="M19.045 30.226l-.003-13.487 12.97 6.767-12.967 6.72z"/>
		</g>
	</symbol>
	<symbol id="email" viewBox="0 0 32 32">
	<path d="M28 3H4a3.957 3.957 0 0 0-4 4v18a3.957 3.957 0 0 0 4 4h24a3.957 3.957 0 0 0 4-4V7a3.957 3.957 0 0 0-4-4zM5.156 24.405l-1.562-1.249 5.271-6.588 1.483 1.348zm21.688 0l-5.192-6.489 1.483-1.348 5.271 6.588zM16.673 19.74a1 1 0 0 1-1.346 0L3.587 9.067l1.346-1.48L16 17.648 27.067 7.587l1.346 1.48z" fill="#535353"/>
	</symbol>
	<symbol id="arrow-down" viewBox="0 0 32 64" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2">
		<path d="M24.891 51.546A1 1 0 0 0 24 51h-7V1a1 1 0 1 0-2 0v50H8a1 1 0 0 0-.809 1.588l8 11a1.001 1.001 0 0 0 1.618 0l8-11a1 1 0 0 0 .082-1.042z" fill="#5b5b5b" fill-rule="nonzero"/>
	</symbol>
	<symbol id="arrow-left" viewBox="0 0 32 32" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2">
		<path d="M8.951 22.39a.718.718 0 0 0 .393-.64V16.72H31.28a.719.719 0 1 0 0-1.438H9.344v-5.03a.718.718 0 0 0-1.142-.582l-7.906 5.75a.72.72 0 0 0 0 1.163l7.906 5.75a.719.719 0 0 0 .75.059z" fill="#636363" fill-rule="nonzero"/>
	</symbol>
	<symbol id="thumb-up" viewBox="0 0 24 24" fill="#268388">
		<path fill="none" d="M0 0h24v24H0V0z"/>
		<path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-2z"/>
	</symbol>
	<symbol id="thumb-down" viewBox="0 0 24 24" fill="#501516">
		<path fill="none" d="M0 0h24v24H0z"/>
		<path d="M15 3H6c-.83 0-1.54.5-1.84 1.22l-3.02 7.05c-.09.23-.14.47-.14.73v2c0 1.1.9 2 2 2h6.31l-.95 4.57-.03.32c0 .41.17.79.44 1.06L9.83 23l6.59-6.59c.36-.36.58-.86.58-1.41V5c0-1.1-.9-2-2-2zm4 0v12h4V3h-4z"/>
	</symbol>

</svg>