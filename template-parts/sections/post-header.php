<?php 
/*----------------------------------------------------------------*\

		POST HEADER
		Display the page title

\*----------------------------------------------------------------*/
?>

<?php if ( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ) : ?>
	<?php 
		//DETERMINE PRODUCT PURCHASED
		$order_id = wc_get_order_id_by_order_key( $_GET['key'] );
		$order = wc_get_order( $order_id );
		$confirmation_page = '887132';
		foreach( $order->get_items() as $item ) {
			if ( 
					$item['product_id'] == 549 || $item['product_id'] == 538 || $item['product_id'] == 848 ||
					$item['product_id'] == 537 || $item['product_id'] == 532 || $item['product_id'] == 531 
				) : 
				//order included a subscription
				$confirmation_page = '887133';
				break; 
			elseif (
					$item['product_id'] == 507 || $item['product_id'] == 515 || $item['product_id'] == 516 || $item['product_id'] == 517 || 
					$item['product_id'] == 520 || $item['product_id'] == 521 || $item['product_id'] == 522 || $item['product_id'] == 523 ||
					$item['product_id'] == 524 || $item['product_id'] == 525 || $item['product_id'] == 526 || 
					$item['product_id'] == 527 || $item['product_id'] == 529 || $item['product_id'] == 530 
				) :
				//order included a gift subscription
				$confirmation_page = '887131';
				break; 
			endif;
		}
	?>
	<header class="post-head">
		<div>
		<?php if ( $order ) : ?>
			<?php if ( $order->has_status( 'failed' ) ) : ?>
				<h1>Transaction Error</h1>
			<?php else : ?>
				<h1><?php echo get_the_title($confirmation_page); ?></h1>
			<?php endif; ?>
		<?php else : ?>
			<h1>Checkout</h1>
		<?php endif; ?>
		</div>
	</header>

<?php else : ?>
	<header class="post-head">
		<div>
			<h1>
				<?php the_title(); ?>
				<?php if ( get_field('subheader') ) : ?>
					<span><?php the_field('subheader'); ?></span>
				<?php endif; ?>
			</h1>
			<?php if ( !is_page_template( 'templates/confirmation.php' ) && !is_page_template( 'templates/order-confirmation.php' ) ) : ?>
				<svg viewBox="0 0 32 64">
					<use xlink:href="#arrow-down"></use>
				</svg>
			<?php endif; ?>
		</div>
	</header>
<?php endif; ?>