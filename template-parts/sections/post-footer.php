<?php 
/*----------------------------------------------------------------*\

		POST FOOTER
		Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<?php 
	$menuParameters = array(
		'container'       => false,
		'echo'            => false,
		'items_wrap'      => '%3$s',
		'depth'           => 0,
		'theme_location' => 'footer_navigation'
	);
?>

<footer class="post-footer">
	<div class="connect">
		<a class="logo" href="<?php echo get_home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-seal.svg" alt="Back to Homepage" />
		</a>
		<nav class="social">
			<a target="_blank" href="https://www.facebook.com/TheGentlemansBox">
				<svg>
					<title>Facebook</title>
					<use xlink:href="#fb" />
				</svg>
			</a>
			<a target="_blank" href="https://twitter.com/gentlemansbox/">
				<svg>
					<title>Twitter</title>
					<use xlink:href="#twitter" />
				</svg>
			</a>
			<a target="_blank" href="https://www.instagram.com/gentlemansbox/">
				<svg>
					<title>Instagram</title>
					<use xlink:href="#insta" />
				</svg>
			</a>
			<a target="_blank" href="https://www.youtube.com/channel/UCRWfsE4lg_KtsXfxii89Ezg">
				<svg>
					<title>Youtube</title>
					<use xlink:href="#youtube" />
				</svg>
			</a>
		</nav>
		<?php echo do_shortcode('[gravityform id=13 title=true description=true]'); ?>
	</div>
	<nav>
		<?php wp_nav_menu( array( 'theme_location' => 'footer_navigation' )); ?>
	</nav>
</footer>
<div class="copyright">
	<p>ęCopyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.<a href="<?php the_permalink(323); ?>"><?php echo get_the_title(323); ?></a><a href="<?php the_permalink(324); ?>"><?php echo get_the_title(324); ?></a><a href="<?php the_permalink(1017279); ?>"><?php echo get_the_title(1017279); ?></a></p>
</div>