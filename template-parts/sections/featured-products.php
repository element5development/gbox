<?php 
/*----------------------------------------------------------------*\

		FEATURED PRODUCTS FROM HOMEPAGE
		Display slider of key products from homepage

\*----------------------------------------------------------------*/
?>

<?php $posts = get_field('featured_products', get_option( 'page_on_front' ) ); ?>
<?php if( $posts ): ?>
	<section class="featured-products">
		<h2>Featured Products</h2>
		<div class="products">
			<?php foreach( $posts as $post): setup_postdata($post); $product = wc_get_product($post); ?>
				<a href="<?php the_permalink(); ?>" class="product">
					<?php if ( $product->is_on_sale() ) : ?>
						<span class="sale">On Sale</span>
					<?php endif; ?>
					<img class="lazyload blur-up" data-expand="100" data-sizes="auto"
						src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'placeholder') ?>" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>"
						data-srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'small'); ?> 350w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?> 750w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?> 1000w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?> 1400w"
						alt="<?php echo $image['alt']; ?>" />
					<h3><?php the_title(); ?></h3>
					<?php if ( $product->is_on_sale() ) : ?>
						<p class="on-sale"><del>$<?php echo $product->get_regular_price(); ?></del>$<?php echo $product->sale_price; ?></p>
					<?php else : ?>
						<p>$<?php echo $product->get_price(); ?></p>
					<?php endif; ?>
				</a>
			<?php endforeach; ?>
		</div>
		<?php if ( !is_shop() ) : ?>
			<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="button">Shop Our Gentleman's Closet</a>
		<?php endif; ?>
	</section>
<?php wp_reset_postdata(); endif ; ?>