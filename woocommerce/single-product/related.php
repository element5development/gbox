<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
?>

<?php if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>

<?php if ( $related_products ) : ?>
	<section class="featured-products">
		<h2>Related Products</h2>
		<?php woocommerce_product_loop_start(); ?>
			<?php foreach ( $related_products as $related_product ) : ?>
				<?php
					$post_object = get_post( $related_product->get_id() );
					setup_postdata( $GLOBALS['post'] =& $post_object );
					$product = wc_get_product($related_product);
				?>
				<a href="<?php the_permalink(); ?>" class="product">
					<?php if ( $product->is_on_sale() ) : ?>
						<span class="sale">On Sale</span>
					<?php endif; ?>
					<img class="lazyload blur-up" data-expand="100" data-sizes="auto"
						src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'placeholder') ?>" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>"
						data-srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'small'); ?> 350w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?> 750w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?> 1000w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?> 1400w"
						alt="<?php echo $image['alt']; ?>" />
					<h3><?php the_title(); ?></h3>
					<?php if ( $product->is_on_sale() ) : ?>
						<p class="on-sale"><del>$<?php echo $product->get_regular_price(); ?></del>$<?php echo $product->sale_price; ?></p>
					<?php else : ?>
						<p>$<?php echo $product->get_price(); ?></p>
					<?php endif; ?>
				</a>
			<?php endforeach; ?>
		<?php woocommerce_product_loop_end(); ?>
	</section>
<?php endif; wp_reset_postdata(); ?>