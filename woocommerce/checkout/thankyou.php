<?php
/**
 * Customized Thankyou page
 * this tempalte has been customized to display different content 
 * based on the products purchased 
 * 1) Subscrition
 * 2) Gift Subscription
 * 3) Other
 */

defined( 'ABSPATH' ) || exit;
?>

<?php 
//DETERMINE PRODUCT PURCHASED
$order_id = wc_get_order_id_by_order_key( $_GET['key'] );
$order = wc_get_order( $order_id );
$confirmation_page = '887132';
foreach( $order->get_items() as $item ) {
	if ( 
			$item['product_id'] == 549 || $item['product_id'] == 538 || $item['product_id'] == 848 ||
			$item['product_id'] == 537 || $item['product_id'] == 532 || $item['product_id'] == 531 
		) : 
		//order included a subscription
		$confirmation_page = '887133';
		break; 
	elseif (
			$item['product_id'] == 507 || $item['product_id'] == 515 || $item['product_id'] == 516 || $item['product_id'] == 517 || 
			$item['product_id'] == 520 || $item['product_id'] == 521 || $item['product_id'] == 522 || $item['product_id'] == 523 ||
			$item['product_id'] == 524 || $item['product_id'] == 525 || $item['product_id'] == 526 || 
			$item['product_id'] == 527 || $item['product_id'] == 529 || $item['product_id'] == 530 
		) :
		//order included a gift subscription
		$confirmation_page = '887131';
		break; 
	endif;
}
?>


<!-- Prodege Marketing Tracking -->
<?php foreach( $order->get_items() as $item ) : ?>
	<?php if ( $item['product_id'] == 531 ) : ?>
		<!-- Offer Goal Conversion: Gentleman\'s Box Classic Subscription(id: 120064) -->
		<img src="https://sboffers.swagbucks.com/GL6ia" width="1" height="1" />
	<?php elseif ( $item['product_id'] == 537 ) : ?>
		<!-- Offer Goal Conversion: Gentleman\'s Box Premium Subscription(id: 120063) -->
		<img src="https://sboffers.swagbucks.com/GL66u" width="1" height="1" />
	<?php endif; ?>
	<?php endforeach; ?>
<!-- /Prodege Marketing Tracking -->

<div class="woocommerce-order">

	<?php if ( $order ) :

		do_action( 'woocommerce_before_thankyou', $order->get_id() ); ?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay', 'woocommerce' ); ?></a>
				<?php if ( is_user_logged_in() ) : ?>
					<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
				<?php endif; ?>
			</p>

		<?php else : ?>

			<?php $image = get_field('image', $confirmation_page); ?>
			<img class="animated-gif lazyload blur-up" data-expand="100" data-sizes="auto" 
				src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
				data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
				alt="<?php echo $image['alt']; ?>"> 

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

			<ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

				<li class="woocommerce-order-overview__order order">
					<?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<li class="woocommerce-order-overview__date date">
					<?php esc_html_e( 'Date:', 'woocommerce' ); ?>
					<strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
					<li class="woocommerce-order-overview__email email">
						<?php esc_html_e( 'Email:', 'woocommerce' ); ?>
						<strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
					</li>
				<?php endif; ?>

				<li class="woocommerce-order-overview__total total">
					<?php esc_html_e( 'Total:', 'woocommerce' ); ?>
					<strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
				</li>

				<?php if ( $order->get_payment_method_title() ) : ?>
					<li class="woocommerce-order-overview__payment-method method">
						<?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
						<strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
					</li>
				<?php endif; ?>

			</ul>

			<?php if( have_rows('next_steps', $confirmation_page) ): ?>
				<section class="next-steps">
					<h2>Next Steps</h2>
					<div class="cards">
						<?php while ( have_rows('next_steps', $confirmation_page) ) : the_row(); ?>
							<div class="card">
								<h3><?php the_sub_field('title', $confirmation_page); ?></h3>
								<p><?php the_sub_field('description', $confirmation_page); ?></p>
								<?php
									$link = get_sub_field('button', $confirmation_page); 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self'; 
								?>
								<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
									<?php echo esc_html($link_title); ?>
								</a>
							</div>
						<?php endwhile; ?>
					</div>
				</section>
			<?php endif; ?>

		<?php endif; ?>

		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>