<?php 
/*----------------------------------------------------------------*\

	Template Name: Order Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php 
	$order_id = apply_filters( 'woocommerce_thankyou_order_id', absint( $_GET['order'] ) );
	$order_key = apply_filters( 'woocommerce_thankyou_order_key', empty( $_GET['key'] ) ? '' : wc_clean( $_GET['key'] ) );
	$order = wc_get_order( $order_id );
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<?php $image = get_field('image'); ?>
		<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
			src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
			data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
			alt="<?php echo $image['alt']; ?>"> 
		<?php if( have_rows('next_steps') ): ?>
			<section class="next-steps">
				<h2>Next Steps</h2>
				<div class="cards">
					<?php while ( have_rows('next_steps') ) : the_row(); ?>
						<div class="card">
							<h3><?php the_sub_field('title'); ?></h3>
							<p><?php the_sub_field('description'); ?></p>
							<?php
								$link = get_sub_field('button'); 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self'; 
							?>
							<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
								<?php echo esc_html($link_title); ?>
							</a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>