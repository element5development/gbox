<?php 
/*----------------------------------------------------------------*\

	Template Name: Home
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php
	$imageLeft = get_field('background_left');
	$imageRight = get_field('background_right');
?>

<header class="post-head front-head">
	<div class="monthly">
		<h1><?php the_field('title_left') ?></h1>
		<p><?php the_field('description_left') ?></p>
		<?php
			$link = get_field('button_left');
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<div class="button" ><?php echo esc_html($link_title); ?></div>
		<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"></a>
		<div class="bg" style="background-image: url(<?php echo $imageLeft['sizes']['xlarge']; ?>);"></div>
	</div>
	<div class="premium">
		<h1><?php the_field('title_right') ?></h1>
		<p><?php the_field('description_right') ?></p>
		<?php
			$link = get_field('button_right');
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';
		?>
		<div class="button" ><?php echo esc_html($link_title); ?></div>
		<a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"></a>
		<div class="bg" style="background-image: url(<?php echo $imageRight['sizes']['xlarge']; ?>);"></div>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if ( get_field('intro') ) : ?>
			<section class="intro">
				<?php the_field('intro'); ?>
				<svg viewBox="0 0 32 64">
					<use xlink:href="#arrow-down"></use>
				</svg>
			</section>
		<?php endif; ?>
		<?php if( have_rows('testimonies') ): ?>
			<section class="testimonies-slider">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php the_field('review_video_compilation'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				<div class="testimonies">
					<?php while ( have_rows('testimonies') ) : the_row(); ?>
						<div class="testimony">
							<div>
								<div>
									<div class="star-rating">
										<?php if ( get_sub_field('stars') == 'one' || get_sub_field('stars') == 'two' || get_sub_field('stars') == 'three' || get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
										<?php endif; ?>
										<?php if ( get_sub_field('stars') == 'two' || get_sub_field('stars') == 'three' || get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
										<?php endif; ?>
										<?php if ( get_sub_field('stars') == 'three' || get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
										<?php endif; ?>
										<?php if ( get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
										<?php endif; ?>
										<?php if ( get_sub_field('stars') == 'five' ) : ?>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
										<?php endif; ?>
									</div>
									<p><?php the_sub_field('testimony'); ?><span>-<?php the_sub_field('name'); ?></span></p>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<!-- <section class="thin-banner">
			<div>
				<svg viewBox="0 0 1224 792" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M1014.77 439.538l-16.924-22.934H796.098l82.048 118.473c-85.36 16.68-165.936-54.699-164.832-137.728 1.227-84.869 69.662-148.398 151.587-147.785 89.897.49 179.55 97.379 149.87 189.974m121.048 145.577s59.85-78.123 60.095-179.794c.491-199.786-139.445-329.542-334.692-329.91-113.077-.122-209.107 55.067-267.362 135.766 24.16 34.34 38.265 67.454 44.029 89.407h-89.407c-4.538 14.104-8.094 28.576-10.67 43.415h110.379a320.356 320.356 0 0 1 3.924 50.039c0 68.434-21.462 131.84-58.01 184.087 56.907 81.803 151.22 141.162 268.22 141.162 77.634 0 132.455-33.114 142.389-38.387l20.604 27.104H1224l-88.18-122.889z" fill="#fff" fill-rule="nonzero"/><path d="M350.168 495.586h82.171c0 3.68-42.066 44.642-105.35 44.642-69.539 0-148.276-54.944-149.87-142.879-1.594-91.124 77.143-150.85 149.87-148.275 58.991 2.085 81.435 19.01 114.426 51.387 0 0 47.585 0 95.907.123h100.444c-5.764-21.953-19.868-55.067-44.029-89.407-2.085-2.82-4.047-5.764-6.254-8.585C539.16 138.204 455.15 72.958 324.536 72.713 171.11 72.468 52.759 175.12 13.759 300.583c-.491 1.472-.982 3.067-1.35 4.539 0 .122 0 .122-.122.245C4.315 333.452-.1 363.132-.1 393.67c.245 29.311 4.047 57.397 11.038 83.888 24.651 91.614 88.67 167.162 173.172 207.757.736.368 1.349.613 2.085.981.368.245.858.368 1.349.613 42.557 19.378 90.388 30.17 141.284 30.538 107.681-.858 202.975-53.595 261.72-134.294 1.227-1.717 2.33-3.434 3.557-5.15 36.548-52.247 58.01-115.653 58.01-184.088 0-17.047-1.349-33.727-3.924-50.038H350.168v151.71z" fill="#fff" fill-rule="nonzero"/></svg>
				<p>Get a 1-Year subscription to GQ magazine <b>FOR FREE</b> when you sign up for Gentleman’s Box</p>
				<button class="join-toggle">Let's Get Savvy</button>
			</div>
		</section> -->
		<section class="blog-categories">
			<h2>Learn What It Means To Be A Gentleman</h2>
			<div class="categories">
				<?php $categories = get_categories(); ?>
				<?php foreach( $categories as $category ) : ?>
					<a href="<?php echo get_category_link( $category->term_id ); ?>" class="category">
						<?php $image = get_field('featured_image', 'category_'.$category->term_id); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>">  
						<h3><?php echo $category->name; ?></h3>
					</a>
				<?php endforeach; ?>
			</div>
			<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="button">Read the Full Gentleman's Guide</a>
		</section>
    <?php if( get_field('gift_title') ): ?>
      <section class="gift-options">
				<div>
					<h2><?php the_field('gift_title'); ?></h2>
					<p><?php the_field('gift_description'); ?></p>
					<svg viewBox="0 0 32 64">
						<use xlink:href="#arrow-down"></use>
					</svg>
					<?php $image = get_field('gift_image'); ?>
					<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
						src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
						data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
						alt="<?php echo $image['alt']; ?>">
					<?php
						$link = get_field('gift_button');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				</div>
			</section>
		<?php endif; ?>
		<?php $posts = get_field('featured_products'); ?>
		<?php if( $posts ): ?>
			<section class="intro featured-products-intro">
				<h2>Upgrade Your Wardrobe</h2>
				<p>Shop The Gentleman’s Closet for add-ons and one-off accessories, fit for any gentleman. </p>
				<svg viewBox="0 0 32 64">
					<use xlink:href="#arrow-down"></use>
				</svg>
			</section>
			<section class="featured-products">
				<div class="products">
					<?php foreach( $posts as $post): setup_postdata($post); $product = wc_get_product($post); ?>
						<a href="<?php the_permalink(); ?>" class="product">
							<?php if ( $product->is_on_sale() ) : ?>
								<span class="sale">On Sale</span>
							<?php endif; ?>
							<img class="lazyload blur-up" data-expand="100" data-sizes="auto"
								src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'placeholder') ?>" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>"
								data-srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'small'); ?> 350w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?> 750w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?> 1000w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?> 1400w"
								alt="<?php echo $image['alt']; ?>" />
							<h3><?php the_title(); ?></h3>
							<?php if ( $product->is_on_sale() ) : ?>
								<p class="on-sale"><del>$<?php echo $product->get_regular_price(); ?></del>$<?php echo $product->sale_price; ?></p>
							<?php else : ?>
								<p>$<?php echo $product->get_price(); ?></p>
							<?php endif; ?>
						</a>
					<?php endforeach; ?>
				</div>
				<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="button">Shop Our Gentleman's Closet</a>
			</section>
		<?php wp_reset_postdata(); endif ; ?>
		<section class="thin-banner">
			<div>
				<p>Create a Unique <b>Groomsmen Gift</b> for Your Wedding</p>
				<a href="<?php the_permalink('413'); ?>" class="button">View Groomsmen Packages</a>
			</div>
		</section>
	</article>
</main>

<?php get_footer(); ?>