<?php 
/*----------------------------------------------------------------*\

	Template Name: Confirmation
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<p><?php the_field('message'); ?></p>
		<?php if( have_rows('cards') ): ?>
			<section class="card-grid text-cards two-columns">
				<?php while ( have_rows('cards') ) : the_row(); ?>
					<div class="card">
						<h3><?php the_sub_field('title') ?></h3>
						<p><?php the_sub_field('description'); ?></p>
						<?php
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
						<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>