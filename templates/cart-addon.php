<?php 
/*----------------------------------------------------------------*\

	Template Name: Cart Addons

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<section class="woocommerce-noticies">
	<?php wc_print_notices() ?>
</section>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<nav>
			<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">
				<svg viewBox="0 0 32 64">
					<use xlink:href="#arrow-left"></use>
				</svg>
				Back to Shop
			</a>
			<?php global $woocommerce; ?>
			<a href="<?php echo $woocommerce->cart->get_checkout_url(); ?>" class="button">Checkout Now</a>
		</nav>
		<?php $posts = get_field('products'); ?>
		<?php if( $posts ): ?>
			<section class="addon-products">
				<div class="products">
					<?php foreach( $posts as $post): setup_postdata($post); $product = wc_get_product($post); ?>
						<div class="product">
							<?php if ( $product->is_on_sale() ) : ?>
								<span class="sale">On Sale</span>
							<?php endif; ?>
							<img class="lazyload blur-up" data-expand="100" data-sizes="auto"
								src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'placeholder') ?>" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>"
								data-srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'small'); ?> 350w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?> 750w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?> 1000w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?> 1400w"
								alt="<?php echo $image['alt']; ?>" />
							<h3><?php the_title(); ?></h3>
							<?php if ( $product->is_on_sale() ) : ?>
								<p class="on-sale"><del>$<?php echo $product->get_regular_price(); ?></del>$<?php echo $product->sale_price; ?></p>
							<?php else : ?>
								<p>$<?php echo $product->get_price(); ?></p>
							<?php endif; ?>
							<a href="<?php echo get_site_url(); ?>/checkout/?add-to-cart=<?php echo get_the_ID() ?>" class="button">Add to Cart</a>
						</div>
					<?php endforeach; ?>
				</div>
			</section>
		<?php wp_reset_postdata(); endif ; ?>
	</article>
</main>

<?php get_footer(); ?>