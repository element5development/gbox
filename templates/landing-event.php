<?php 
/*----------------------------------------------------------------*\

	Template Name: Event/Holiday
	
\*----------------------------------------------------------------*/
?>
<?php get_header(); ?>

<header class="post-head">
	<div>
		<h1>
			<?php the_title(); ?>
			<?php if ( get_field('subheader') ) : ?>
				<span><?php the_field('subheader'); ?></span>
			<?php endif; ?>
		</h1>
		<svg viewBox="0 0 32 64">
			<use xlink:href="#arrow-down"></use>
		</svg>
	</div>
</header>

<main>
	<article>
		<?php if( have_rows('cards') ): ?>
			<section class="product-cards">
				<div class="options">
					<?php while ( have_rows('cards') ) : the_row(); ?>
						<div class="option">
							<?php $image = get_sub_field('icon'); ?>
							<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
								src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
								data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
								alt="<?php echo $image['alt']; ?>">  
							<h2><?php the_sub_field('title'); ?></h2>
							<p><?php the_sub_field('description'); ?></p>
							<?php 
                $link = get_sub_field('button'); 
                $link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
              ?>
              <a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( get_field('coupon_content') ): ?>
			<section class="coupon">
				<?php $image = get_field('coupon_image'); ?>
				<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
					src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
					data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
					alt="<?php echo $image['alt']; ?>">  
				<p><?php the_field('coupon_content'); ?></p>
			</section>
		<?php endif; ?>
		<section class="community">
			<h2>Thousands of Gentlemen Looking Dapper</h2>
			<?php if( have_rows('community_cards', 418) ): ?>
				<div class="community-gallery">
					<?php while ( have_rows('community_cards', 418) ) : the_row(); ?>
						<div class="member">
							<?php $image = get_sub_field('image'); ?>
							<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
								src="<?php echo $image['sizes']['medium']; ?>" 
								alt="<?php echo $image['alt']; ?>" />  	
							<p>
								<svg viewBox="0 0 24 24">
									<use xlink:href="#insta"></use>
								</svg>
								<?php the_sub_field('instagram_handle'); ?>
							</p>
						</div>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
		</section>
		<?php if( have_rows('media_content') ): ?>
			<?php while ( have_rows('media_content') ) : the_row(); ?>
				<section class="media_content">
					<?php $image = get_sub_field('image'); ?>
					<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
						src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
						data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
						alt="<?php echo $image['alt']; ?>">  
					<div class="content">
						<div>
							<?php the_sub_field('content'); ?>
						</div>
					</div>
				</section>
			<?php endwhile; ?>
		<?php endif; ?>
		<section class="support">
			<h2><?php the_field('support_title'); ?></h2>
			<div class="buttons">
				<a href="javascript:$zopim.livechat.window.show();" class="button">Chat With Support</a>
				<a href="tel:+12484796066" class="button">Call (248) 479-6066</a>
				<a href="mailto:begood@gentlemansbox.com" class="button">begood@gentlemansbox.com</a>
			</div>
		</section>
	</article>
</main>

<?php get_footer(); ?>