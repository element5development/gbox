<?php 
/*----------------------------------------------------------------*\

	Template Name: Single Goal
	
\*----------------------------------------------------------------*/
?>
<?php get_header(); ?>

<header class="post-head">
	<div>
		<h1>
			<?php the_title(); ?>
			<?php if ( get_field('subheader') ) : ?>
				<span><?php the_field('subheader'); ?></span>
			<?php endif; ?>
		</h1>
		<?php
			$link = get_field('button'); 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self'; 
		?>
		<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
			<?php echo esc_html($link_title); ?>
		</a>
		<svg viewBox="0 0 32 64">
			<use xlink:href="#arrow-down"></use>
		</svg>
	</div>
</header>

<main>
	<article>
		<?php if( have_rows('cards') ): ?>
			<section class="cards">
				<?php while ( have_rows('cards') ) : the_row(); ?>
					<div class="card">
						<?php $image = get_sub_field('icon'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>">  
						<h2><?php the_sub_field('title'); ?></h2>
						<p><?php the_sub_field('description'); ?></p>
					</div>
				<?php endwhile; ?>
				<?php
					$link = get_field('cards_button'); 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self'; 
				?>
				<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
					<?php echo esc_html($link_title); ?>
				</a>
			</section>
		<?php endif; ?>
		<?php if( have_rows('tiers') ): ?>
			<section class="tiers">
				<?php while ( have_rows('tiers') ) : the_row(); ?>
					<div class="tier">
						<?php if( get_sub_field('most_popular') ): ?>
							<div class="badge">
								Most Popular
							</div>
						<?php endif; ?>
						<h2><?php the_sub_field('title'); ?></h2>
						<?php $image = get_sub_field('image'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<div class="description">
							<?php the_sub_field('description'); ?> 
						</div>
						<?php
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self'; 
						?>
						<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
						<p><?php the_sub_field('legal'); ?></p>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
		<?php if( have_rows('media_text') ): ?>
			<section class="media-text">
				<?php while ( have_rows('media_text') ) : the_row(); ?>
					<div class="row">
						<?php $image = get_sub_field('image'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<div>
							<?php the_sub_field('content'); ?>
						</div>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
		<?php if( get_field('banner') ): ?>
			<section class="banner">
				<h2><?php the_field('banner'); ?></h2>
				<?php
					$link = get_field('banner_button'); 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self'; 
				?>
				<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
					<?php echo esc_html($link_title); ?>
				</a>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>