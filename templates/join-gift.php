<?php 
/*----------------------------------------------------------------*\

	Template Name: Gift
	
\*----------------------------------------------------------------*/
?>
<?php get_header(); ?>

<header class="post-head">
	<div>
		<?php $image = get_field('example_gift_box'); ?>
		<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
			src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
			data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
			alt="<?php echo $image['alt']; ?>">				
		<h1>
			<?php the_title(); ?>
			<?php if ( get_field('subheader') ) : ?>
				<span><?php the_field('subheader'); ?></span>
			<?php endif; ?>
		</h1>
		<a class="button" href="#shipping">Let's Get Gifting</a>
	</div>
</header>

<main>
	<article>
		<nav>
			<a href="#shipping"><span>Shipping</span></a>
			<a href="#price"><span>Pricing</span></a>
			<a href="#style"><span>Products</span></a>
		</nav> 
		<section id="shipping">
			<div>
				<h2>
					Where will you be shipping your gift?
				</h2>
				<div class="options">
					<div class="option">
						<a class="ship-us-toggle button" href="#price">Within United States</a>
					</div>
					<div class="option">
						<a class="ship-can-toggle button" href="#price">Within Canada</a>
					</div>
					<div class="option">
						<a class="ship-int-toggle button" href="#price">Elsewhere</a>
					</div>
				</div>
			</div>
		</section>
		<section id="price">
			<div>
				<h2>
					How much are you looking to spend?
				</h2>
				<div class="options is-active">
					<div class="option">
						<a class="pricing-one button" href="#style">Under $100</a>
					</div>
					<div class="option">
						<a class="pricing-two button" href="#style">$100 - $200</a>
					</div>
					<div class="option">
						<a class="pricing-three button" href="#style">$200 - $300</a>
					</div>
					<div class="option">
						<a class="pricing-four button" href="#style">Over $300</a>
					</div>
				</div>
			</div>
		</section>
		<section id="style">
			<div>
				<h2>
					Which items best fit your gift?
				</h2>
				<div class="options is-active">
					<div class="option">
						<div class="gallery">
							<?php $images = get_field('classic_gallery'); ?>
							<?php foreach( $images as $image ): ?>
								<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
									src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
									data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
									alt="<?php echo $image['alt']; ?>">  
							<?php endforeach; ?>
						</div>
						<a class="style-classic button" href="#subscriptions">Fashion Accessories</a>
					</div>
					<div class="option">
						<div class="gallery">
							<?php $images = get_field('premium_gallery'); ?>
							<?php foreach( $images as $image ): ?>
								<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
									src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
									data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
									alt="<?php echo $image['alt']; ?>">  
							<?php endforeach; ?>
						</div>
						<a class="style-premium button" href="#subscriptions">Lifestyle Products</a>
						<p>These items are currently only avilable in United States and Canada.</p>
					</div>
				</div>
			</div>
		</section>
		<section id="subscriptions">
			<div>
				<div class="recommendations">
					<div class="recommendation" data-type="classic" data-shipping="us" data-pricing="1">
						<div>our recommendation</div>
						<?php $image = get_field('classic_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>">  
						<h3>3 boxes for $99</h3>
						<p>Receive a new box filled with classy and modern stylish accessories and lifestyle essentials every month for the next three months. It’s the gift that keeps on giving and for the next three month’s you will receive over $300 in value.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=524" class="button">Give 3 boxes</a>
					</div>
					<div class="recommendation" data-type="classic" data-shipping="us" data-pricing="2">
						<div>our recommendation</div>
						<?php $image = get_field('classic_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 6 boxes for $189</h3>
						<p>Receive a new box filled with classy and modern stylish accessories and lifestyle essentials every month for the next six months. It’s the gift that keeps on giving and for the next six month’s you will receive over $600 in value.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=525" class="button">Give 6 boxes</a>
					</div>
					<div class="recommendation" data-type="classic" data-shipping="us" data-pricing="3 4">
						<div>our recommendation</div>
						<?php $image = get_field('classic_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 12 boxes for $349</h3>
						<p>Save money with our most popular Classic Gift Option! Receive a new box filled with classy and modern stylish accessories and lifestyle essentials every month for an ENTIRE YEAR. It’s the gift that keeps on giving and for the next twelve month’s you will receive over $1200 in value.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=526" class="button">Give 12 boxes</a>
					</div>
					<div class="recommendation" data-type="classic" data-shipping="can int" data-pricing="1">
						<div>our recommendation</div>
						<?php $image = get_field('classic_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 3 boxes for $111</h3>
						<p>Receive a new box filled with classy and modern stylish accessories and lifestyle essentials every month for the next three months. It’s the gift that keeps on giving and for the next three month’s you will receive over $300 in value.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=527" class="button">Give 3 boxes</a>
					</div>
					<div class="recommendation" data-type="classic" data-shipping="can int" data-pricing="2 3">
						<div>our recommendation</div>
						<?php $image = get_field('classic_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 6 boxes for $213</h3>
						<p>Receive a new box filled with classy and modern stylish accessories and lifestyle essentials every month for the next six months. It’s the gift that keeps on giving and for the next six month’s you will receive over $600 in value.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=529" class="button">Give 6 boxes</a>
					</div>
					<div class="recommendation" data-type="classic" data-shipping="can int" data-pricing="4">
						<div>our recommendation</div>
						<?php $image = get_field('classic_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 12 boxes for $397</h3>
						<p>Save money with our most popular Classic Gift Option! Receive a new box filled with classy and modern stylish accessories and lifestyle essentials every month for an ENTIRE YEAR. It’s the gift that keeps on giving and for the next twelve month’s you will receive over $1200 in value.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=530" class="button">Give 12 boxes</a>
					</div>
					<div class="recommendation" data-type="premium" data-shipping="us" data-pricing="1 2">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 1 box for $129</h3>
						<p>Start out gifting one Premium box valued at over $300. With this package, the recipient will receive one premium box for the quarter. If you wish to gift more boxes over a longer period of time, change your preferences above and save some money.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=517" class="button">Give 1 box</a>
					</div>
					<div class="recommendation" data-type="premium" data-shipping="us" data-pricing="3">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 2 boxes for $238</h3>
						<p>Gift two premium boxes over the course of 6 months. That’s right, the recipient of this gift option will receive two boxes, one new box every three months. Each box will be curated differently to ensure they won’t receive the same product.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=516" class="button">Give 2 Boxes</a>
					</div>
					<!-- <div class="recommendation" data-type="premium" data-shipping="us" data-pricing="4">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 3 boxes for $327</h3>
						<p>Diam mi accumsan ac tincidunt phasellus. Consequat, proin nunc vivamus integer facilisis porttitor in congue. Sit egestas eget sapien tellus pharetra interdum. Porttitor vel nascetur dictumst neque, eros, viverra sed nisi, nisl. Congue interdum morbi nullam aliquam.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=515" class="button">Give 3 Boxes</a>
					</div> -->
					<div class="recommendation" data-type="premium" data-shipping="us"data-pricing="4">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 4 boxes for $396</h3>
						<p>The most popular Premium Gift option is our Annual gift. Receive 4 new boxes, one box every three months for an entire year. Each box is carefully curated to fit the season and the theme each quarter. With this gift package, you receive over $1200 in lifestyle products.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=507" class="button">Give 4 Boxes</a>
					</div>
					<div class="recommendation" data-type="premium" data-shipping="can" data-pricing="1 2">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 1 box for $159</h3>
						<p>Start out gifting one Premium box valued at over $300. With this package, the recipient will receive one premium box for the quarter. If you wish to gift more boxes over a longer period of time, change your preferences above and save some money.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=520" class="button">Give 1 box</a>
					</div>
					<div class="recommendation" data-type="premium" data-shipping="can" data-pricing="3">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 2 boxes for $298</h3>
						<p>Gift two premium boxes over the course of 6 months. That’s right, the recipient of this gift option will receive two boxes, one new box every three months. Each box will be curated differently to ensure they won’t receive the same product.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=521" class="button">Give 2 Boxes</a>
					</div>
					<!-- <div class="recommendation" data-type="premium" data-shipping="can" data-pricing="4">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 3 boxes for $417</h3>
						<p>Diam mi accumsan ac tincidunt phasellus. Consequat, proin nunc vivamus integer facilisis porttitor in congue. Sit egestas eget sapien tellus pharetra interdum. Porttitor vel nascetur dictumst neque, eros, viverra sed nisi, nisl. Congue interdum morbi nullam aliquam.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=522" class="button">Give 3 Boxes</a>
					</div> -->
					<div class="recommendation" data-type="premium" data-shipping="can" data-pricing="4">
						<div>our recommendation</div>
						<?php $image = get_field('premium_featured'); ?>
						<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
							src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
							data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
							alt="<?php echo $image['alt']; ?>"> 
						<h3>Give 4 boxes for $516</h3>
						<p>The most popular Premium Gift option is our Annual gift. Receive 4 new boxes, one box every three months for an entire year. Each box is carefully curated to fit the season and the theme each quarter. With this gift package, you receive over $1200 in lifestyle products.</p>
						<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>/?add-to-cart=523" class="button">Give 4 Boxes</a>
					</div>
				</div>
				<div class="explination">
					<h2>Answer the question above to find which gift is right for you.</h2>
					<p>
						<span></span> • <span></span> • <span></span>
					</p>
					<h2>
						<span></span> Gift Subscriptions
					</h2>
					<div class="classic-description is-hidden">
						<?php the_field('classic_description'); ?>
					</div>
					<div class="premium-description is-hidden">
						<?php the_field('premium_description'); ?>
					</div>
				</div>
			</div>
		</section>
	</article>
</main>

<?php get_footer(); ?>