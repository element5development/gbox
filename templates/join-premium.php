<?php 
/*----------------------------------------------------------------*\

	Template Name: Premium
	
\*----------------------------------------------------------------*/
?>
<?php get_header(); ?>

<?php $image = get_field('background_image'); ?>

<header class="post-head lazyload" data-expand="-150" data-bgset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w" data-sizes="auto">
	<div>
		<h1>
			<?php the_title(); ?>
			<?php if ( get_field('subheader') ) : ?>
				<span><?php the_field('subheader'); ?></span>
			<?php endif; ?>
		</h1>
		<a href="https://gentlemansbox.com/choose-your-plan/" class="button is-red">Start Your Membership</a>
		<p>Over $200 in value in each box.<br/>Free shipping to US customers.</p>
		<svg viewBox="0 0 32 64">
			<use xlink:href="#arrow-down"></use>
		</svg>
	</div>
</header>

<main>
	<article>
		<?php if ( get_field('intro') ) : ?>
			<section class="intro">
				<div>
					<?php the_field('intro'); ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( have_rows('highlight_cards') ): ?>
			<section class="highlight-cards">
				<div class="cards">
					<?php while ( have_rows('highlight_cards') ) : the_row(); ?>
						<div class="card">
							<?php $image = get_sub_field('icon'); ?>
							<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
								src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
								data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
								alt="<?php echo $image['alt']; ?>">  
							<h2><?php the_sub_field('title'); ?></h2>
							<p><?php the_sub_field('description'); ?></p>
						</div>
					<?php endwhile; ?>
				</div>
				<a href="https://gentlemansbox.com/choose-your-plan/" class="button">Join Today</a>
			</section>
		<?php endif; ?>
		<?php if( have_rows('testimonies') ): ?>
			<section class="testimonies-wrapper">
				<?php $image = get_field('testimony_image'); ?>
				<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
					src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" 
					data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  
					alt="<?php echo $image['alt']; ?>">  
				<div class="testimonies">
					<?php while ( have_rows('testimonies') ) : the_row(); ?>
						<div class="testimony">
							<div class="star-rating">
								<?php if ( get_sub_field('stars') == 'one' || get_sub_field('stars') == 'two' || get_sub_field('stars') == 'three' || get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
								<?php endif; ?>
								<?php if ( get_sub_field('stars') == 'two' || get_sub_field('stars') == 'three' || get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
								<?php endif; ?>
								<?php if ( get_sub_field('stars') == 'three' || get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
								<?php endif; ?>
								<?php if ( get_sub_field('stars') == 'four' || get_sub_field('stars') == 'five' ) : ?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
								<?php endif; ?>
								<?php if ( get_sub_field('stars') == 'five' ) : ?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M19.735 12L16 0l-3.735 12H0l9.774 7.341-3.854 11.86L16 23.879l10.08 7.322-3.854-11.86L32 12H19.735z"/></svg>
								<?php endif; ?>
							</div>
							<p><?php the_sub_field('testimony'); ?><span>- <?php the_sub_field('name'); ?></span></p>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if( have_rows('process') ): $x = 0; ?>
			<section class="process">
				<h2>How It Works</h2>
				<?php while ( have_rows('process') ) : the_row(); $x++; ?>
					<section class="process-step">
						<div>
							<div class="text">
								<h3><?php the_sub_field('title'); ?></h3>
								<p><?php the_sub_field('description'); ?></p>
							</div>
							<div class="image">
								<?php $image = get_sub_field('image'); ?>
								<img class="lazyload blur-up" data-expand="100" data-sizes="auto" 
									src="<?php echo $image['sizes']['medium']; ?>" 
									alt="<?php echo $image['alt']; ?>">  
								<span><?php echo $x; ?>.</span>
							</div>
						</div>
					</section>
				<?php endwhile; ?>
				<a href="https://gentlemansbox.com/choose-your-plan/" class="button">Join Today</a>
			</section>
		<?php endif; ?>
		<?php if( have_rows('previous_boxes') ): ?>
			<section class="past-boxes">
				<h2>What Gentlemen Received In Past Boxes</h2>
				<div class="boxes">
					<?php while ( have_rows('previous_boxes') ) : the_row(); ?>
						<div class="box">
							<?php $image = get_sub_field('image'); ?>
							<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['medium']; ?>" data-src="<?php echo $image['sizes']['medium']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 750w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1400w"  alt="<?php echo $image['alt']; ?>">
							<div class="text">
								<h3><?php the_sub_field('title'); ?></h3>
								<a href="https://gentlemansbox.com/choose-your-plan/" class="button is-red">Join Today</a>
								<p>Over $200 in value in each box.</p>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
		<!-- <section class="thin-banner">
			<div>
				<svg viewBox="0 0 1224 792" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2"><path d="M1014.77 439.538l-16.924-22.934H796.098l82.048 118.473c-85.36 16.68-165.936-54.699-164.832-137.728 1.227-84.869 69.662-148.398 151.587-147.785 89.897.49 179.55 97.379 149.87 189.974m121.048 145.577s59.85-78.123 60.095-179.794c.491-199.786-139.445-329.542-334.692-329.91-113.077-.122-209.107 55.067-267.362 135.766 24.16 34.34 38.265 67.454 44.029 89.407h-89.407c-4.538 14.104-8.094 28.576-10.67 43.415h110.379a320.356 320.356 0 0 1 3.924 50.039c0 68.434-21.462 131.84-58.01 184.087 56.907 81.803 151.22 141.162 268.22 141.162 77.634 0 132.455-33.114 142.389-38.387l20.604 27.104H1224l-88.18-122.889z" fill="#fff" fill-rule="nonzero"/><path d="M350.168 495.586h82.171c0 3.68-42.066 44.642-105.35 44.642-69.539 0-148.276-54.944-149.87-142.879-1.594-91.124 77.143-150.85 149.87-148.275 58.991 2.085 81.435 19.01 114.426 51.387 0 0 47.585 0 95.907.123h100.444c-5.764-21.953-19.868-55.067-44.029-89.407-2.085-2.82-4.047-5.764-6.254-8.585C539.16 138.204 455.15 72.958 324.536 72.713 171.11 72.468 52.759 175.12 13.759 300.583c-.491 1.472-.982 3.067-1.35 4.539 0 .122 0 .122-.122.245C4.315 333.452-.1 363.132-.1 393.67c.245 29.311 4.047 57.397 11.038 83.888 24.651 91.614 88.67 167.162 173.172 207.757.736.368 1.349.613 2.085.981.368.245.858.368 1.349.613 42.557 19.378 90.388 30.17 141.284 30.538 107.681-.858 202.975-53.595 261.72-134.294 1.227-1.717 2.33-3.434 3.557-5.15 36.548-52.247 58.01-115.653 58.01-184.088 0-17.047-1.349-33.727-3.924-50.038H350.168v151.71z" fill="#fff" fill-rule="nonzero"/></svg>
				<p>Get a 1-Year subscription to GQ magazine <b>for free</b> when you sign up for Gentleman’s Box</p>
				<a href="https://gentlemansbox.com/choose-your-plan/" class="button">Let's Get Savvy</a>
			</div>
		</section> -->
		<section class="support">
			<h2>Need A helping hand?</h2>
			<div class="buttons">
				<a href="javascript:$zopim.livechat.window.show();" class="button">Chat With Support</a>
				<a href="tel:+12484796066" class="button">Call (248) 479-6066</a>
				<a href="mailto:begood@gentlemansbox.com" class="button">begood@gentlemansbox.com</a>
			</div>
		</section>
	</article>
</main>

<?php get_footer(); ?>