<?php 
/*----------------------------------------------------------------*\

	SEARCH RESULTS ARCHIVE

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<header class="post-head">
	<div>
		<h1><?php echo 'Search results for: ' . get_search_query(); ?></h1>
		<svg viewBox="0 0 32 64">
			<use xlink:href="#arrow-down"></use>
		</svg>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<?php	while ( have_posts() ) : the_post(); ?>
				<article class="search-result <?php echo get_post_type(); ?>">
					<header>
						<a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
					</header>
					<div class="entry-content">
						<?php the_excerpt(); ?>
						<a href="<?php the_permalink(); ?>" class="button">Read More</a>
					</div>
				</article>
			<?php endwhile; ?>
		<?php else : ?>
			<section class="is-narrow">
				<h2>We cannot find anything for "<?php echo(get_search_query()); ?>".<br>Please try another term.</h2>
				<?php echo get_search_form(); ?>
			</section>
		<?php endif; ?>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_footer(); ?>