<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<header class="post-head">
	<div>
		<h1>A little lost? Let's try again.</h1>
	</div>
</header>

<main id="main-content">
	<article>
			<section class="card-grid text-cards two-columns">
				<div class="card">
					<h3>Start From the Beginning</h3>
					<p>Seems this page has no content. Let's try starting from the begining to find what your looking for.</p>
					<a class="button" href="<?php echo get_site_url(); ?>">View Home Page</a>
				</div>
				<div class="card">
					<h3>Start Shopping</h3>
					<p>Seems this page has no content. Might be faster to reset all filters and view all our products.</p>
					<a class="button" href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">View Our Products</a>
				</div>
			</section>
	</article>
</main>

<?php get_footer(); ?>