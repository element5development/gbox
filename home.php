<?php 
/*----------------------------------------------------------------*\

		DEFAULT ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<? //get current page we are on. If not set we can assume we are on page 1.
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
?>

<?php get_header(); ?>

<header class="post-head">
	<div>
		<h1><?php the_field('post_title','options'); ?></h1>
		<?php if (get_field('post_intro','options')) : ?>
			<p><?php the_field('post_intro','options'); ?></p>
		<?php endif; ?>
		<svg viewBox="0 0 32 64">
			<use xlink:href="#arrow-down"></use>
		</svg>
	</div>
</header>

<main id="main-content">
	<article>
		<?php if(1 == $paged) : ?>
			<?php	$i = 0; while ( have_posts() ) : the_post(); ?>
				<?php if ( $i < 1 ) : ?>
					<section class="latest-post">
						<h2>Our Latest Post</h2>
						<article class="archive-result">
							<a href="<?php the_permalink(); ?>">
								<img class="lazyload blur-up" data-expand="100" data-sizes="auto"
									src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'placeholder') ?>" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>"
									data-srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'small'); ?> 350w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?> 750w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?> 1000w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?> 1400w"
									alt="<?php echo $image['alt']; ?>" />
							</a>
							<div class="text">
								<header>
									<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
									<div class="meta">
										<span class="date"><?php echo get_the_date() ?></span><?php foreach((get_the_category()) as $category) : ?><a href="<?php echo get_category_link( $category->term_id ); ?>" class="category"><?php echo $category->name; ?></a>
										<?php endforeach; ?>
									</div>
								</header>
								<a href="<?php the_permalink(); ?>" class="entry-content">
									<?php the_excerpt(); ?>
									<span>Read More...</span>
								</a>
							</div>
						</article>
					</section>
				<?php endif; ?>
			<?php $i++; endwhile; ?>
			<section class="blog-categories">
				<h2>Learn What It Means To Be A Gentleman</h2>
				<div class="categories">
					<?php $categories = get_categories(); ?>
					<?php foreach( $categories as $category ) : ?>
						<a href="<?php echo get_category_link( $category->term_id ); ?>" class="category">
							<?php $image = get_field('featured_image', 'category_'.$category->term_id); ?>
							<img data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['small']; ?>" alt="<?php echo $image['alt']; ?>" />
							<h3><?php echo $category->name; ?></h3>
						</a>
					<?php endforeach; ?>
				</div>
			</section>
		<?php endif; ?>
		<section class="additional-posts">
			<?php if(1 == $paged) : ?>
				<h2>More Posts</h2>
				<svg viewBox="0 0 32 64">
					<use xlink:href="#arrow-down"></use>
				</svg>
			<?php endif; ?>
			<div class="blog-feed">
				<?php	while ( have_posts() ) : the_post(); ?>
					<article class="archive-result">
						<a href="<?php the_permalink(); ?>">
							<img class="lazyload blur-up" data-expand="100" data-sizes="auto"
								src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'placeholder') ?>" data-src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>"
								data-srcset="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'small'); ?> 350w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?> 750w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'large'); ?> 1000w, <?php echo get_the_post_thumbnail_url(get_the_ID(), 'xlarge'); ?> 1400w"
								alt="<?php echo $image['alt']; ?>" />
						</a>
						<div class="text">
							<header>
								<a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
								<div class="meta">
									<span class="date"><?php echo get_the_date() ?></span><?php foreach((get_the_category()) as $category) : ?><a href="<?php echo get_category_link( $category->term_id ); ?>" class="category"><?php echo $category->name; ?></a>
									<?php endforeach; ?>
								</div>
							</header>
						</div>
					</article>
				<?php endwhile; ?>
			</div>
		</section>
	</article>
	<?php clean_pagination(); ?>
</main>

<?php get_footer(); ?>