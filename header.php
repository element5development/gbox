<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!doctype html>
<html xml:lang="en" lang="en">

<head>
	<!-- Start of gentlemansbox Zendesk Widget script -->
	<script>/*<![CDATA[*/window.zE||(function(e,t,s){var n=window.zE=window.zEmbed=function(){n._.push(arguments)}, a=n.s=e.createElement(t),r=e.getElementsByTagName(t)[0];n.set=function(e){ n.set._.push(e)},n._=[],n.set._=[],a.async=true,a.setAttribute("charset","utf-8"), a.src="https://static.zdassets.com/ekr/asset_composer.js?key="+s, n.t=+new Date,a.type="text/javascript",r.parentNode.insertBefore(a,r)})(document,"script","de2a87c8-7349-47f6-81ed-c885972459b4");/*]]>*/</script>
	<!-- End of gentlemansbox Zendesk Widget script -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name="msvalidate.01" content="D7FF62B50B5AC4465FB383958CD4A132" />
	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7VV3KH');</script>
<!-- End Google Tag Manager -->
	<!-- Facebook Pixel Code --
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '726903087431698'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=726903087431698&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
	<!-- Facebook Add to Cart Event --
<!-- 361 is Cart and 886953 is Cart Addons --
<?php if( is_page(361) || is_page(886953) ) { ?>
<script>fbq('track', 'AddToCart');</script>
<?php } ?>
<!-- End Facebook Add to Cart Event --
<!-- Facebook Initiate Checkout Event --
<!-- 362 is Checkout --
<?php if( is_page(362) ) { ?>
<script>fbq('track', 'InitiateCheckout');</script>
<?php } ?>
<!-- End Facebook Initiate Checkout Event -->
	<!-- Facebook Complete Purchase Event --
<?php if( is_checkout() && is_wc_endpoint_url('order-received') ) { ?>
<?php $order_id = wc_get_order_id_by_order_key( $_GET['key'] ); $order = wc_get_order( $order_id ); ?>
<?php if ( $order ) : ?>
<?php $total = $order->get_total(); $orderid = $order->get_order_number(); ?>
<?php else : ?>
<?php $total = "0.00"; $orderid	= "0"; ?>
<?php endif; ?>
<script>fbq('track', 'Purchase', {value: <?php echo $total ?>, currency: 'USD', order_id: '<?php echo $orderid ?>'});</script>
<?php } ?>
<!-- End Facebook Complete Purchase Event -->
	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		 if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		 n.queue=[];t=b.createElement(e);t.async=!0;
		 t.src=v;s=b.getElementsByTagName(e)[0];
		 s.parentNode.insertBefore(t,s)}(window, document,'script',
										 'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '369131131061644');
		fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
				   src="https://www.facebook.com/tr?id=369131131061644&ev=PageView&noscript=1"
				   /></noscript>
	<!-- End Facebook Pixel Code -->

	<!-- TapJoy Subscription Purchase Event -->
	<?php if( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ) { ?>
	<img src="https://tapjoy.go2cloud.org/SL2Zy" width="1" height="1" />
	<?php } ?>
	<!-- End TapJoy Subscription Purchase Event -->

	<!-- SmarterChaos Universal Tracking Tag -->
	<script type="text/javascript"> 
		(function(a,b,c,d,e,f,g){e['ire_o']=c;e[c]= e[c]||function(){(e[c].a=e[c].a||[]).push(arguments)};f=d.createElement(b);g=d.getElementsByTagName(b)[0];f.async=1;f.src=a;g.parentNode.insertBefore(f,g);})('//d.impactradius-event.com/A2204730-7624-4981-9413-2e499b8f36dc1.js','script','ire',document,window); 
	</script>
	<!-- End SmarterChaos Universal Tracking Tag -->

	<!-- moving the domain name -->
	<meta name=“google-site-verification” content=“nqh2w5jfkFXLFmn9m6Qiuo1ti7PpXmCfnoiVFWcsDlU” />
	<!-- moving the domain name -->
</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7VV3KH"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	
	<!-- Octane AI widget -->
	<script src="https://octaneai.com/dl53q1j2awao7ayg/customerchat.js" async defer></script>
	<!-- Octane AI widget -->

	<!-- SmarterChaos Identifier script tag -->
	<?php
		global $current_user;
		get_currentuserinfo();
		$email = sha1($current_user->user_email);
		$current_user_id = get_current_user_id();
	?>
	<script type="text/javascript"> 
		ire('identify', { customerId: <?php echo $current_user_id; ?>, customerEmail: '<?php echo $email; ?>' });
	</script>
	<!-- End SmarterChaos Identifier script tag -->

	<!-- SmarterChaos Action Tracker -->
	<?php if ( is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ) : ?>
		<?php
			// true = existing || false = new
			$customerStatus = has_bought( $order->get_customer_id() );
			// Coupons used in the order LOOP (as they can be multiple)
			$coupons = array();
			$couponsValues = array();
			foreach( $order->get_coupon_codes() as $coupon_code ){
				$couponName = $coupon_code;
				$coupon_post_obj = get_page_by_title($coupon_code, OBJECT, 'shop_coupon');
				$coupon_id       = $coupon_post_obj->ID;
				$coupon = new WC_Coupon($coupon_id);

				if ( $coupon->get_discount_type() == 'percent' ) {
					$coupon_amount = '"' . number_format($coupon->amount, 2) . '%"';
				} else {
					$coupon_amount = '"$' . number_format($coupon->amount, 2) . '"';
				}
				// Add coupon to array
				$coupons[] = '"'.$couponName.'"';
				$couponsValues[] = $coupon_amount;
			}
			//products
			$products = array();
			$product_details = array();
			$order_items = $order->get_items();
			foreach( $order_items as $product ) {
				$productID = $product->get_product_id();
				$productX = wc_get_product($productID);
				$prodcutSku = '"'.$productX->get_sku().'"';
				if( $productX->is_on_sale() ) {
					$productValue = number_format(get_post_meta( $productID, '_sale_price', true), 2);
				} else {
					$productValue = number_format(get_post_meta( $productID, '_regular_price', true), 2);
				}
				$productValue = number_format($product->get_total(), 2);
				$productQty = $product->get_quantity();
				$productCat = array();
				$terms = get_the_terms( $productID, 'product_cat' );
				foreach ( $terms as $term ) {
					$product_cat_slug = $term->slug;
					$productCat[] = $product_cat_slug;
				}
				$productSummary = array( 
					'productID' => $productID, 
					'prodcutSku' => $prodcutSku, 
					'productValue' => $productValue,
					'productQty' => $productQty,
					'productCats' => '"'.implode(' | ', $productCat).'"'
				);
				$products[] = $productSummary;
			}

			$orderPromoCode = implode(' | ', $coupons);
			if ( empty ($orderPromoCode) ) {
				$orderPromoCode = "noCoupon";
			}

			$orderDiscount = implode(' | ', $couponsValues);
			if ( empty ($orderDiscount) ) {
				$orderDiscount = "0.00";
			}
		?>
		<script type="text/javascript">
			ire('trackConversion', 20580, { 
				orderId: <?php echo '"'.$order->get_id().'"'; ?>,         
				customerId: <?php echo '"'.$order->get_customer_id().'"'; ?>, 
				customerStatus: <?php echo '"'.$customerStatus.'"'; ?>, // 1 = Existing & 0 = New
				orderPromoCode: <?php echo $orderPromoCode; ?>,
				// orderDiscount: <?php echo $orderDiscount; ?>,
				currencyCode: "USD", 
				items: [
					<?php foreach ( $products as $prod ) { ?>
						{
							subTotal: <?php echo $prod['productValue']; ?>, 
							category: <?php echo $prod['productCats']; ?>, 
							sku: <?php echo $prod['prodcutSku']; ?>, 
							quantity: <?php echo $prod['productQty']; ?>
						},
					<?php } ?>
				] 
			});
		</script>
	<?php endif; ?>
	<!-- End SmarterChaos Action Tracker -->

	<a id="skip-to-content" href="#main-content">Skip to main content</a>

	<?php get_template_part('template-parts/icon-set'); ?>

	<?php get_template_part('template-parts/elements/navigation'); ?>

	<div class="cookie-useage-notification">
		<div>
			<button>
				<svg>
					<use xlink:href="#close" />
				</svg>
			</button>
			<p>This site uses cookies to provide you with a greater user experience. By using our website, you accept our <a href="<?php the_permalink(323) ?>/#cookie-notification">use of cookies</a>.</p>
		</div>
	</div>