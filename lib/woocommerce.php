<?php
/*----------------------------------------------------------------*\
		INITIALIZE WOOCOMMERCE
\*----------------------------------------------------------------*/
function theme_woocommerce_support() {
	add_theme_support( 'woocommerce', array(
		'thumbnail_image_width' => 250,
		'gallery_thumbnail_image_width' => 250,
		'single_image_width'    => 600,
			'product_grid'          => array(
					'default_rows'    => 4,
					'min_rows'        => 2,
					'max_rows'        => 8,
					'default_columns' => 4,
					'min_columns'     => 2,
					'max_columns'     => 5,
			),
	) );
}
add_action( 'after_setup_theme', 'theme_woocommerce_support' );
/*----------------------------------------------------------------*\
		DISABLE BREADCRUMBS
\*----------------------------------------------------------------*/
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
/*----------------------------------------------------------------*\
		DISABLE WOO PAGNATION
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
add_action( 'woocommerce_after_shop_loop', 'clean_pagination', 20); 
/*----------------------------------------------------------------*\
		REMOVE SORT BY
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
/*----------------------------------------------------------------*\
		REMOVE RESULTS COUNT
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );
/*----------------------------------------------------------------*\
		MOVE SIDEBAR LOCATION
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
add_action( 'woocommerce_after_shop_loop', 'woocommerce_get_sidebar', 10); 
/*----------------------------------------------------------------*\
		ENABLE LIGHTBOX
\*----------------------------------------------------------------*/
// function woo_gallery_setup() {
// 	add_theme_support( 'wc-product-gallery-lightbox' );
// }
// add_action( 'after_setup_theme', 'woo_gallery_setup' );
/*----------------------------------------------------------------*\
		REMOVE PRODUCT IMAGE LINKS
\*----------------------------------------------------------------*/
function remove_product_image_link( $html, $post_id ) {
	return preg_replace( "!<(a|/a).*?>!", '', $html );
}
add_filter( 'woocommerce_single_product_image_thumbnail_html', 'remove_product_image_link', 10, 2 );
/*----------------------------------------------------------------*\
		ADD CHAINED PRODUCTS BELOW PRIMARY IMAGE
\*----------------------------------------------------------------*/
function chained_products() {
	if ( has_term( 'bundles', 'product_cat' ) ) : 
		echo "<h3 class='included'>Products In This Bundle</h3>";
		echo do_shortcode('[chained_products]');
	endif;
}
add_action( 'woocommerce_after_single_product_summary', 'chained_products', 20 );
/*----------------------------------------------------------------*\
		SALE TEXT CHANGE
\*----------------------------------------------------------------*/
function vs_change_sale_content($content, $post, $product){
  $content = '<span class="sale">'.__( 'On Sale', 'woocommerce' ).'</span>';
  return $content;
}
add_filter('woocommerce_sale_flash', 'vs_change_sale_content', 10, 3);
/*----------------------------------------------------------------*\
		REMOVE STAR RATING BELOW TITLE
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
/*----------------------------------------------------------------*\
		NUMBER OF PRODUCTS PER PAGE
\*----------------------------------------------------------------*/
function new_loop_shop_per_page( $cols ) {
  $cols = 12;
  return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
/*----------------------------------------------------------------*\
		HIDE SUBSCRIPTIONS AND GIFT SUBSCRIPTIONS FROM SHOP
\*----------------------------------------------------------------*/
function hide_products_category_shop( $q ) {
    $tax_query = (array) $q->get( 'tax_query' );
    $tax_query[] = array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'subscriptions', 'gift-subscriptions'),
			'operator' => 'NOT IN'
    );
    $q->set( 'tax_query', $tax_query );
}
add_action( 'woocommerce_product_query', 'hide_products_category_shop' );
/*----------------------------------------------------------------*\
		REMOVE SHORT DESCRIPTION
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
/*----------------------------------------------------------------*\
		REMOVE PRODUCT META
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
/*----------------------------------------------------------------*\
		ADD SHARE LINKS
\*----------------------------------------------------------------*/
function social_share_icons() {
	get_template_part('template-parts/elements/share');
}
add_action( 'woocommerce_single_product_summary', 'social_share_icons', 30 );
/*----------------------------------------------------------------*\
		REMOVE TABS
\*----------------------------------------------------------------*/
function woo_remove_product_tabs( $tabs ) {
	unset( $tabs['description'] );          
	unset( $tabs['reviews'] );          
	unset( $tabs['additional_information'] );   
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
/*----------------------------------------------------------------*\
		RELOCATE DESCRIPTION
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
add_action( 'woocommerce_single_product_summary', 'the_content', 20 );
/*----------------------------------------------------------------*\
		RELOCATE REVIEWS
\*----------------------------------------------------------------*/
add_action( 'woocommerce_after_single_product_summary', 'comments_template', 30 );
/*----------------------------------------------------------------*\
		REMOVE STOCK ~ updated as client wanted the 'out of stock' label
\*----------------------------------------------------------------*/
// add_filter( 'woocommerce_get_stock_html', '__return_empty_string' );
/*----------------------------------------------------------------*\
		RELOCATE RELATED PRODUCTS
\*----------------------------------------------------------------*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 40 );
/*----------------------------------------------------------------*\
		NUMBER OF RELATED PRODUCTS
\*----------------------------------------------------------------*/
function related_products_args( $args ) {
	$args['posts_per_page'] = 10; // 4 related products
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'related_products_args', 20 );
/*----------------------------------------------------------------*\
		REDIRECT TO CUSTOM THANK YOU
		this function broke analytic tracking so was replaced with
		customized thankyou.php template and called the confirmation
		content by page ID
\*----------------------------------------------------------------*/
// function redirect_depending_on_product_id(){
// 	/* do nothing if we are not on the appropriate page */
// 	if( !is_wc_endpoint_url( 'order-received' ) || empty( $_GET['key'] ) ) {
// 		return;
// 	}
// 	$order_id = wc_get_order_id_by_order_key( $_GET['key'] );
// 	$order = wc_get_order( $order_id );
// 	$confirmation = 0;
// 	foreach( $order->get_items() as $item ) {
// 		if ( 
// 				$item['product_id'] == 549 || $item['product_id'] == 538 || $item['product_id'] == 848 ||
// 				$item['product_id'] == 537 || $item['product_id'] == 532 || $item['product_id'] == 531 
// 			) : 
// 			//order included a subscription
// 			$confirmation = 1;
// 			break; 
// 		elseif (
// 				$item['product_id'] == 507 || $item['product_id'] == 515 || $item['product_id'] == 516 || $item['product_id'] == 517 || 
// 				$item['product_id'] == 520 || $item['product_id'] == 521 || $item['product_id'] == 522 || $item['product_id'] == 523 ||
// 				$item['product_id'] == 524 || $item['product_id'] == 525 || $item['product_id'] == 526 || 
// 				$item['product_id'] == 527 || $item['product_id'] == 529 || $item['product_id'] == 530 
// 			) :
// 			//order included a gift subscription
// 			$confirmation = 2;
// 			break; 
// 		endif;
// 	}
// 	if ( $confirmation == 1 ) : 
// 		wp_redirect( 'https://gentlemanbox.wpengine.com/checkout/subscription-approved/' );
// 		exit;
// 	elseif ( $confirmation == 2 ) :
// 		wp_redirect( 'https://gentlemanbox.wpengine.com/checkout/gift-subscription/' );
// 		exit;
// 	else :
// 		wp_redirect( 'https://gentlemanbox.wpengine.com/checkout/order-placed/' );
// 		exit;
// 	endif;
// }
// add_action( 'template_redirect', 'redirect_depending_on_product_id' );
/*----------------------------------------------------------------*\
		SHIPPING ADDRESS ENABLED BY DEFAULT
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
function shipchange( $translated_text, $text, $domain ) {
	switch ( $translated_text ) {
	case 'Ship to a different address?' :
		$translated_text = __( 'Ship to a different address', 'woocommerce' );
		break;
	}
	return $translated_text;
}
add_filter('gettext', 'shipchange', 20, 3);
/*----------------------------------------------------------------*\
		REMOVE SUBSCRIBER OPTIONS
\*----------------------------------------------------------------*/
function remove_subscriptions_options( $actions, $subscription ) {
	foreach ( $actions as $action_key => $action ) {
		switch ( $action_key ) {
//		case 'change_payment_method':		// Hide "Change Payment Method" button?
//		case 'change_address':					// Hide "Change Address" button?
			case 'switch':									// Hide "Switch Subscription" button?
 			case 'resubscribe':							// Hide "Resubscribe" button from an expired or cancelled subscription?
//		case 'pay':											// Hide "Pay" button on subscriptions that are "on-hold" as they require payment?
//		case 'reactivate':							// Hide "Reactive" button on subscriptions that are "on-hold"?
			case 'cancel':									// Hide "Cancel" button on subscriptions that are "active" or "on-hold"?
				unset( $actions[ $action_key ] );
				break;
			default: 
				break;
		}
	}
	return $actions;
}
add_filter( 'wcs_view_subscription_actions', 'remove_subscriptions_options', 100, 2 );
/*------------------------------------------*\
		REMOVE SKIP FOR GIFT SUBSCRIPTIONS
\*------------------------------------------*/
function disable_suspend_for_gifts( $actions, $subscription ) {
	// Get the products in the subscription
	$items = $subscription->get_items(); 
	// Get the ID for each product
	$ids = array();
	foreach( $items as $item ) {
		$product = $item->get_product();
		$product_id = $product->get_id();
		$ids[] = $product_id;
	}
	// Get the category for each product ID
	$terms = array();
	foreach( $ids as $id ) {
		$cats = get_the_terms($id, 'product_cat');
		$terms[] = $cats[0]->term_id;
	}
	// If its a gift subscription hide actions
	if ( in_array("43", $terms) ) {
    foreach ( $actions as $action_key => $action ) {
      switch ( $action_key ) {
				case 'suspend':			// Hide "Suspend" button
      		unset( $actions[ $action_key ] );
      		break;
        default: 
          break;
      }
    }
  }
	return $actions;
}
add_filter( 'wcs_view_subscription_actions', 'disable_suspend_for_gifts', 100, 2);

/*----------------------------------------------------------------*\
		SHIPPING ADDRESS HEADLINE
\*----------------------------------------------------------------*/
add_filter('gettext', 'translate_reply');
add_filter('ngettext', 'translate_reply');
function translate_reply($translated) {
	$translated = str_ireplace('Ship to a different address?', 'Shipping Address', $translated);
	return $translated;
}
/*----------------------------------------------------------------*\
		AUTO CHECK DIFFERENT ADDRESS
\*----------------------------------------------------------------*/
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_true' );
/*----------------------------------------------------------------*\
		SHIPSTATION FIRST CLASSIC BOX VARIABLE
\*----------------------------------------------------------------*/
	// function shipstation_custom_field_3() {
	// 	return 'Select Your First Classic Box'; 
	// }
	// add_filter( 'woocommerce_shipstation_export_custom_field_3', 'shipstation_custom_field_3' );
/*----------------------------------------------------------------*\
		SHIPSTATION FIRST PREMIUM BOX VARIABLE
\*----------------------------------------------------------------*/
// function shipstation_custom_field_2() {
// 	return 'Select Your First Premium Box'; 
// }
// add_filter( 'woocommerce_shipstation_export_custom_field_2', 'shipstation_custom_field_2' );
/*----------------------------------------------------------------*\
		SHIPSTATION GIFT VARIABLE
\*----------------------------------------------------------------*/
	function shipstation_custom_field_1() {
		return 'Gift Message'; 
	}
	add_filter( 'woocommerce_shipstation_export_custom_field_1', 'shipstation_custom_field_1' );
/*----------------------------------------------------------------*\
		LEGAL PRIVACY MESSAGE FOR CHECKOUT
\*----------------------------------------------------------------*/
// function legal_privacy_checkout_message() {
//   echo '<div class="woocommerce-info legal-message">If you wish to cancel before your next billing date, the cancellation request must be received by customer support at least 1 full business day prior to billing. If your billing date falls over a weekend or holiday, the cancellation request must be received before 12:00pm EST on the previous business day. Our cancellation customer service hours are Monday – Friday 9:00am to 8:00pm EST. You can cancel your subscription by giving us a call at (248)-479-6066.</div>';
// }
// add_action( 'woocommerce_review_order_before_payment', 'legal_privacy_checkout_message', 10 );
/*----------------------------------------------------------------*\
		CHANGE REQUIRED FIELDS
\*----------------------------------------------------------------*/
// add_filter( 'woocommerce_billing_fields', 'remove_phone_requirement', 10, 1 );
// function remove_phone_requirement( $address_fields ) {
// 		$address_fields['billing_phone']['required'] = false;
//     return $address_fields;
// }
// add_filter( 'woocommerce_billing_fields', 'remove_town_requirement', 10, 1 );
// function remove_town_requirement( $address_fields ) {
// 		$address_fields['billing_city']['required'] = false;
// 		return $address_fields;
// }
// add_filter( 'woocommerce_billing_fields', 'remove_address_requirement', 10, 1 );
// function remove_address_requirement( $address_fields ) {
// 		$address_fields['billing_address_1']['required'] = false;
// 		return $address_fields;
// }
/*----------------------------------------------------------------*\
		SET DEFAULT COUNTRY FOR CHECKOUT
\*----------------------------------------------------------------*/
// add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );
// function change_default_checkout_country() {
//   return 'US'; // country code
// }
// add_filter( 'default_checkout_shippinh_country', 'change_default_shipping_country' );
// function change_default_shipping_country() {
//   return 'US'; // country code
// }
/*----------------------------------------------------------------*\
		REMOVE FREE TRIAL TEXT
\*----------------------------------------------------------------*/
function wc_subscriptions_custom_price_string( $pricestring ) {
	global $product;

	if ( has_term( array( 'gift-subscriptions' ), 'product_cat', $product->id ) ): 
		//remove trial text for gift subscriptions
		$newprice =  str_replace("with a 10-day free trial","",$pricestring);
	else :	
		//remove trial and signup for standard subscriptions
		$newprice = strstr($pricestring, 'with', true) ?: $pricestring;
	endif;
	return $newprice;
}
add_filter( 'woocommerce_subscriptions_product_price_string', 'wc_subscriptions_custom_price_string' );
add_filter( 'woocommerce_subscription_price_string', 'wc_subscriptions_custom_price_string' );
/*----------------------------------------------------------------*\
		redirect to checkout after adding product to cart
\*----------------------------------------------------------------*/
function redirect_checkout_add_cart() {
   return wc_get_checkout_url();
}
add_filter( 'woocommerce_add_to_cart_redirect', 'redirect_checkout_add_cart' );
/*----------------------------------------------------------------*\
		determine if this is a first time order
		true = existing || false = new
\*----------------------------------------------------------------*/
function has_bought() {
	// Get all customer orders
	if ( is_user_logged_in() ) : 
		$customer_orders = get_posts( array(
				'numberposts' => -1,
				'meta_key'    => '_customer_user',
				'meta_value'  => get_current_user_id(),
				'post_type'   => 'shop_order', // WC orders post type
				'post_status' => 'wc-completed' // Only orders with status "completed"
		) );
		// return "true" when customer has already at least one order (false if not)
		if ( count($customer_orders) > 0 ) :
			return 1;
		else : 
			return 0;
		endif;
 	else : 
		return 0;
 	endif; 
}
/*----------------------------------------------------------------*\
		CUSTOM RETRY RULES
\*----------------------------------------------------------------*/
function my_custom_retry_rules() {
	return array(
		array(
				'retry_after_interval'            => 259200,
				'email_template_customer'         => 'WCS_Email_Customer_Payment_Retry', 
				'email_template_admin'            => '',
				'status_to_apply_to_order'        => 'pending',
				'status_to_apply_to_subscription' => 'on-hold',
		),
		array(
				'retry_after_interval'            => 172800,
				'email_template_customer'         => '', // avoid spamming the customer by not sending them an email this time either
				'email_template_admin'            => '',
				'status_to_apply_to_order'        => 'pending',
				'status_to_apply_to_subscription' => 'on-hold',
		),
		array(
				'retry_after_interval'            => 432000,
				'email_template_customer'         => 'WCS_Email_Customer_Payment_Retry', 
				'email_template_admin'            => '',
				'status_to_apply_to_order'        => 'pending',
				'status_to_apply_to_subscription' => 'on-hold',
		),
		array(
				'retry_after_interval'            => 432000,
				'email_template_customer'         => 'WCS_Email_Customer_Payment_Retry', 
				'email_template_admin'            => '',
				'status_to_apply_to_order'        => 'pending',
				'status_to_apply_to_subscription' => 'on-hold',
		),
		array(
				'retry_after_interval'            => 172800,
				'email_template_customer'         => '', // avoid spamming the customer by not sending them an email this time either
				'email_template_admin'            => 'WCS_Email_Payment_Retry',
				'status_to_apply_to_order'        => 'pending',
				'status_to_apply_to_subscription' => 'on-hold',
		),
	);
}
add_filter( 'wcs_default_retry_rules', 'my_custom_retry_rules' );
/*----------------------------------------------------------------*\
		ADDRESS WAIRNING MESSAGE
\*----------------------------------------------------------------*/
function customer_support_content() {
	echo '<p class="support-message">Please note this will not change the shipping address on your subscription(s). If you wish to change the shipping address for future renewals please edit your subscription details found under "Subscriptions".</p><hr>';
}
add_action( 'woocommerce_before_edit_account_address_form', 'customer_support_content' );