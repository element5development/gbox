<?php
/*----------------------------------------------------------------*\
		DISABLE BLOCKS FOR SPECIFIC PAGES AND TEMPLATES
\*----------------------------------------------------------------*/
function disable_editor_by_template( $id = false ) {
	$excluded_templates = array(
		'templates/join-premium.php',
		'templates/join-classic.php',
		'templates/join-gift.php',
		'templates/comparison.php',
		'templates/single-goal-landing.php',
		'templates/landing-event.php',
		'templates/cart-addon.php',
		'templates/order-confirmation.php',
		'templates/confirmation.php',
		'templates/home.php'
	);
	if( empty( $id ) )
		return false;
	$id = intval( $id );
	$template = get_page_template_slug( $id );
	return in_array( $template, $excluded_templates );
}
function disable_gutenberg_by_template( $can_edit, $post_type ) {
	if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
		return $can_edit;
	if( disable_editor_by_template( $_GET['post'] ) )
		$can_edit = false;
	return $can_edit;
}
add_filter( 'gutenberg_can_edit_post_type', 'disable_gutenberg_by_template', 10, 2 );
add_filter( 'use_block_editor_for_post_type', 'disable_gutenberg_by_template', 10, 2 );
/*----------------------------------------------------------------*\
	DISABLE BLOCK CUSTOM COLORS AND FONT SIZES
\*----------------------------------------------------------------*/
function gutenberg_disable_custom_options() {
	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'disable-custom-font-size' );
}
add_action( 'after_setup_theme', 'gutenberg_disable_custom_options' );

/*----------------------------------------------------------------*\
		DEFAULT BLOCK STYLES
\*----------------------------------------------------------------*/
add_theme_support( 'wp-block-styles' );

/*----------------------------------------------------------------*\
		"WIDE" AND "FULL" SIZE OPTIONS
\*----------------------------------------------------------------*/
add_theme_support( 'align-wide' );

/*----------------------------------------------------------------*\
		RESPONSIVE EMBEDDED BLOCKS
\*----------------------------------------------------------------*/
add_theme_support( 'responsive-embeds' );

/*----------------------------------------------------------------*\
		BLOCK FONT SIZES
\*----------------------------------------------------------------*/
add_theme_support( 'editor-font-sizes', array(
	array(
		'name'      => __( 'X-Small' ),
		'shortName' => __( 'XS' ),
		'size'      => 12,
		'slug'      => 'xs',
	),
	array(
		'name'      => __( 'Small' ),
		'shortName' => __( 'S' ),
		'size'      => 14,
		'slug'      => 'small',
	),
	array(
		'name'      => __( 'Medium' ),
		'shortName' => __( 'M' ),
		'size'      => 16,
		'slug'      => 'medium',
	),
	array(
		'name'      => __( 'Large' ),
		'shortName' => __( 'L' ),
		'size'      => 18,
		'slug'      => 'large',
	),
	array(
		'name'      => __( 'X-large' ),
		'shortName' => __( 'XL' ),
		'size'      => 20,
		'slug'      => 'xl',
	),
) );

/*----------------------------------------------------------------*\
		BLOCK COLOR PALETTE
\*----------------------------------------------------------------*/
add_theme_support('editor-color-palette', array(
	array(
		'name'  => __( 'White' ),
		'slug'  => 'white',
		'color' => '#ffffff',
	),
	array(
		'name'  => __( 'Light Gray' ),
		'slug'  => 'light-gray',
		'color' => '#eaeaea',
	),
	array(
		'name'  => __( 'Gray' ),
		'slug'  => 'gray',
		'color' => '#9b9b9b',
	),
	array(
		'name'  => __( 'Dark Gray' ),
		'slug'  => 'dark-gray',
		'color' => '#636363',
	),
	array(
		'name'  => __( 'Black' ),
		'slug'  => 'black',
		'color' => '#000000',
	),
	array(
		'name'  => __( 'Tan' ),
		'slug'  => 'tan',
		'color' => '#CCB49C',
	),
	array(
		'name'  => __( 'Blue' ),
		'slug'  => 'blue',
		'color' => '#2A4D69',
	),
	array(
		'name'  => __( 'Red' ),
		'slug'  => 'red',
		'color' => '#501516
		',
	),
) );