<?php
/*----------------------------------------------------------------*\
		INITIALIZE WIDGET AREA
\*----------------------------------------------------------------*/
function footer_widget_areas() {
	$args = array(
		'name'          => __( 'shop' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);

	$args = array(
		'name'          => __( 'footer-menu' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);

	$args = array(
		'name'          => __( 'footer-contact' ),
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4>',
		'after_title'   => '</h4>',
	);
	register_sidebar($args);
}
add_action( 'widgets_init', 'footer_widget_areas' );