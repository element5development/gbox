<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE
	page template which takes advantage of the WordPress block system,
	however some default blocks and settings have been removed.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/sections/post-header'); ?>

<main id="main-content">
	<article>
		<?php the_content(); ?>
	</article>
</main>

<?php get_footer(); ?>